//
// Created by nickeskov on 27.12.2019.
//

#include <boost/log/trivial.hpp>

#include "boost_trivial_logger.h"


void BoostTrivialLogger::trace(const std::string &str) {
    BOOST_LOG_TRIVIAL(trace) << str;
}

void BoostTrivialLogger::debug(const std::string &str) {
    BOOST_LOG_TRIVIAL(debug) << str;
}

void BoostTrivialLogger::info(const std::string &str) {
    BOOST_LOG_TRIVIAL(info) << str;
}

void BoostTrivialLogger::warning(const std::string &str) {
    BOOST_LOG_TRIVIAL(warning) << str;
}

void BoostTrivialLogger::error(const std::string &str) {
    BOOST_LOG_TRIVIAL(error) << str;
}

void BoostTrivialLogger::fatal(const std::string &str) {
    BOOST_LOG_TRIVIAL(fatal) << str;
}
