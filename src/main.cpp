//
// Created by nickeskov on 26.12.2019.
//

#include "ilogger.h"
#include "boost_trivial_logger.h"
#include "configuration.hpp"
#include "lb.h"

#include "group_server.h"
#include <iostream>
#include <string>
#include <memory>


int main_cycle() {
    std::string command;
    while(true) {
        std::cin >> command;

        if (command == "stop") {
            return 0;
        } else if (command == "reload") {
            return 1;
        } else {
            std::cout << "Unknown command, try again" << std::endl;
        }
    }
}


std::vector<GroupServer> start_server(const std::string &config_path
        , const std::shared_ptr<ILogger>& logger) {

    Configuration config;
    std::vector<GroupServer> servers;

    auto config_parse_status = config.Set(config_path);
    if (config_parse_status.code != return_code::SUCCESS) {
        logger->error(config_parse_status.message);
        return servers;
    }

    if (config.get_groups()) {
        std::vector<Group> groups = *(config.get_groups());
        for (auto group : groups) {
            servers.emplace_back(group, logger);
        }
    }
    return servers;
}

int main(int argc, char **argv) {
    std::shared_ptr<ILogger> logger = std::make_shared<BoostTrivialLogger>();
    if (argc < 2) {
        logger->error("Invalid arguments count.");
        return 1;
    }

    auto config = Configuration();
    std::vector<GroupServer> servers;
    std::string config_path(argv[1]);

    while (true) {
        servers = start_server(config_path, logger);
        if (servers.empty()) {
            return 3;
        }

        for (auto &server : servers) {
            server.run();
        }

        int command_code = main_cycle();
        if (command_code == 0) {
            return 0;
        } else if (command_code == 1) {
            for (auto &server : servers) {
                server.stop();
            }
            servers.clear();
            continue;
        }
    }
    return 0;
}
