//
// Created by nickeskov on 26.12.2019.
//

#include "backends_dispenser.h"
#include "host_holder.h"
#include "noxy_utils.h"

#include <utility>

BackendsDispenser::BackendsDispenser(std::shared_ptr<ilb> load_balancer)
    : _load_balancer(std::move(load_balancer)), _mutex() { }

std::unique_ptr<IHostHolder> BackendsDispenser::dispense() {
    std::lock_guard<std::mutex> lock(_mutex);
    return std::make_unique<HostHolder>(utils::to_host_holder(_load_balancer->get()));
}

void BackendsDispenser::set_balancer(std::shared_ptr<ilb> load_balancer) {
    std::lock_guard<std::mutex> lock(_mutex);
    _load_balancer = std::move(load_balancer);
}
