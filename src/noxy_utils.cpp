//
// Created by nickeskov on 26.12.2019.
//

#include <algorithm>
#include "noxy_utils.h"

namespace utils {
    HostHolder to_host_holder(const std::string &str) {
        std::size_t port_separator_pos = str.find_last_of(':');
        std::string port = str.substr(port_separator_pos + 1);
        std::string ip = str.substr(0, port_separator_pos);
        auto for_remove = std::remove_if(ip.begin(), ip.end(), [](char c) {
            return c == ']' || c == '[';
        });
        bool is_ipv6 = for_remove != ip.end();
        ip.erase(for_remove, ip.end());
        return HostHolder(ip, std::stoi(port), is_ipv6);
    }
}