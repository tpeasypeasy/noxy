//
// Created by nickeskov on 27.12.2019.
//

#include <future>

#include "backends_dispenser.h"
#include "noxy_utils.h"
#include "lb.h"
#include "group_server.h"



namespace {
    namespace algorithm_name {
        constexpr auto round_robin = "round_robin";
        constexpr auto weighted_round_robin = "weighted_round_robin";
        constexpr auto smooth_round_robin = "smooth_round_robin";
        constexpr auto random = "random";
    }

    balancing_method get_balance_algorithm_type(const std::string &str) {
        auto alg_type = static_cast<balancing_method>(-1);

        if (str == algorithm_name::round_robin) {
            alg_type = balancing_method::RR;
        } else if (str == algorithm_name::weighted_round_robin) {
            alg_type = balancing_method::WRR;
        } else if (str == algorithm_name::smooth_round_robin) {
            alg_type = balancing_method::SWRR;
        } else if (str == algorithm_name::random) {
            alg_type = balancing_method::RAND;
        }

        return alg_type;
    }

    std::vector<std::shared_ptr<EpollEngine>> engines_from_group(Group &group,
            const std::shared_ptr<ILogger> &logger) {
        std::vector<std::shared_ptr<EpollEngine>> engines;
        engines.reserve(group.get_servers().size());

        std::vector<std::shared_ptr<backend>> backends;
        backends.reserve(group.get_back().size());

        for (const auto &[back_host_str, weight] : group.get_back()) {
            backends.push_back(std::make_shared<backend>(back_host_str, weight, false));
        }

        auto alg_type = get_balance_algorithm_type(group.get_algorithm());
        if (alg_type == static_cast<balancing_method>(-1)) {
            logger->warning("Unknown algotithm in group " + group.get_name() + ", use default=random");
            alg_type = balancing_method::RAND;
        }

        auto dispencer = std::make_shared<BackendsDispenser>(
                std::make_shared<lb>(backends, static_cast<balancing_method>(alg_type)));

        for (const auto &proxy_interface : group.get_servers()) {
            auto proxy_interface_holder = utils::to_host_holder(proxy_interface);
            engines.push_back(std::make_shared<EpollEngine>(proxy_interface_holder, dispencer, logger));
        }

        return engines;
    }
}

struct GroupServer::Impl {
  public:
    Impl(Group &group, const std::shared_ptr<ILogger> &logger)
        : engines(engines_from_group(group, logger))
        , logger(logger) { }
    std::vector<std::shared_ptr<EpollEngine>> engines;
    std::shared_ptr<ILogger> logger;
    std::vector<std::thread> runners;
};

GroupServer::GroupServer(Group &group, const std::shared_ptr<ILogger> &logger)
    : _impl(std::make_unique<GroupServer::Impl>(group, logger)) { }

GroupServer::GroupServer(GroupServer &&other) noexcept
        : _impl(std::move(other._impl)) { }

GroupServer &GroupServer::operator=(GroupServer &&other) noexcept {
    _impl = std::move(other._impl);
    return *this;
}

void GroupServer::run() {
    _impl->runners.reserve(_impl->engines.size());
    for (auto &engine : _impl->engines) {
        _impl->runners.emplace_back(std::thread([engine]() {
            engine->run();
       }));
    }
}

void GroupServer::stop() {
    for (auto &engine : _impl->engines) {
        engine->stop();
    }
    for (auto &runner : _impl->runners) {
        // TODO(nickeskov): !!!!!!!!!!!!!!!!
        runner.join();
    }
    _impl->runners.clear();
}

GroupServer::~GroupServer() {
    if (_impl) {
        GroupServer::stop();
    }
}
