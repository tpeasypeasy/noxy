//
// Created by master on 21.12.2019.
//
#include <gtest/gtest.h>
#include <lb.h>

TEST(swrr_test, diff_weight) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 5, false)),
            std::shared_ptr<backend>(new backend("b", 1, false)),
            std::shared_ptr<backend>(new backend("c", 1, false)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::SWRR));
    std::vector<std::string> expected = {"a", "a", "b", "a", "c", "a", "a"};
    for (int i = 0; i < 7; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }
}

TEST(swrr_test, same_weight) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 1, false)),
            std::shared_ptr<backend>(new backend("b", 1, false)),
            std::shared_ptr<backend>(new backend("c", 1, false)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::SWRR));
    std::vector<std::string> expected = {"a", "b", "c", "a", "b", "c"};
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }
}

TEST(swrr_test, same_not_one_weight) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 2, false)),
            std::shared_ptr<backend>(new backend("b", 2, false)),
            std::shared_ptr<backend>(new backend("c", 2, false)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::SWRR));
    std::vector<std::string> expected = {"a", "b", "c", "a", "b", "c"};
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }
}

TEST(wrr_test, diff_weight) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 5, false)),
            std::shared_ptr<backend>(new backend("b", 1, false)),
            std::shared_ptr<backend>(new backend("c", 1, false)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::WRR));
    std::vector<std::string> expected = {"b", "c", "a", "a", "a", "a", "a", "b", "c", "a", "a", "a", "a"};

    for (int i = 0; i < 13; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }

}

TEST(rr_test, same_weight) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 5, false)),
            std::shared_ptr<backend>(new backend("b", 5, false)),
            std::shared_ptr<backend>(new backend("c", 5, false)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::RR));
    std::vector<std::string> expected = {"b", "c", "a", "b", "c", "a",};
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }
}

TEST(rr_test, diff_weight) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 5, false)),
            std::shared_ptr<backend>(new backend("b", 1, false)),
            std::shared_ptr<backend>(new backend("c", 1, false)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::RR));
    std::vector<std::string> expected = {"b", "c", "a", "b", "c", "a",};
    for (int i = 0; i < 6; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }
}

TEST(rr_test, down_be) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 5, false)),
            std::shared_ptr<backend>(new backend("b", 1, false)),
            std::shared_ptr<backend>(new backend("c", 1, true)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::RR));
    std::vector<std::string> expected = {"b", "a", "b", "a", "b",};
    for (int i = 0; i < 5; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }
    load->set_down(1);
    expected = {"a", "a", "a", "a", "a"};
    for (int i = 0; i < 5; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }
    load->set_down(0);
    expected = {"", "", "", "", ""};
    for (int i = 0; i < 5; i++) {
        ASSERT_EQ(load->get(), expected[i]);
    }
}

TEST(wrr_test, down_be) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 1, false)),
            std::shared_ptr<backend>(new backend("b", 1, false)),
            std::shared_ptr<backend>(new backend("c", 1, true)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::WRR));
    for (int i = 0; i < 5; i++) {
        ASSERT_FALSE("c" == load->get());
    }
    load->set_down(1);
    for (int i = 0; i < 5; i++) {
        ASSERT_FALSE("c" == load->get());
        ASSERT_FALSE("b" == load->get());
    }
    load->set_down(0);
    for (int i = 0; i < 5; i++) {
        ASSERT_EQ(load->get(), "");
    }
}

TEST(swrr_test, down_be) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 7, false)),
            std::shared_ptr<backend>(new backend("b", 6, false)),
            std::shared_ptr<backend>(new backend("c", 5, true)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::SWRR));
    for (int i = 0; i < 50; i++) {
        ASSERT_FALSE("c" == load->get());
    }
    load->set_down(1);
    for (int i = 0; i < 50; i++) {
        ASSERT_FALSE("c" == load->get());
        ASSERT_FALSE("b" == load->get());
    }
    load->set_down(0);
    for (int i = 0; i < 5; i++) {
        ASSERT_EQ(load->get(), "");
    }
}

TEST(rand_test, down_be) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 7, false)),
            std::shared_ptr<backend>(new backend("b", 6, false)),
            std::shared_ptr<backend>(new backend("c", 5, true)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::RAND));
    for (int i = 0; i < 50; i++) {
        ASSERT_FALSE("c" == load->get());
    }
    load->set_down(1);
    for (int i = 0; i < 50; i++) {
        ASSERT_FALSE("c" == load->get());
        ASSERT_FALSE("b" == load->get());
    }
    load->set_down(0);
    for (int i = 0; i < 5; i++) {
        ASSERT_EQ(load->get(), "");
    }
}


TEST(rand_test, up_be) {
    std::vector<std::shared_ptr<backend>> be = {
            std::shared_ptr<backend>(new backend("a", 7, true)),
            std::shared_ptr<backend>(new backend("b", 6, true)),
            std::shared_ptr<backend>(new backend("c", 5, true)),
    };
    std::shared_ptr<lb> load(new lb(be, balancing_method::RAND));
    for (int i = 0; i < 5; i++) {
        ASSERT_EQ(load->get(), "");
    }
    load->set_up(0);
    for (int i = 0; i < 50; i++) {
        ASSERT_FALSE("c" == load->get());
        ASSERT_FALSE("b" == load->get());
    }
    load->set_up(1);
    for (int i = 0; i < 50; i++) {
        ASSERT_FALSE("c" == load->get());
    }
    

    load->set_down(0);

}


int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
