//
// Created by master on 21.12.2019.
//
#include <gtest/gtest.h>
#include <lb.h>

const double EPS_SWRR = 0.000000001, EPS_WRR = 0.1;

const int be_num = 1000;

TEST(stress, swrr) {
    std::vector<std::shared_ptr<backend>> be;
    unsigned int total = 0;
    for (int i = 0; i < be_num; i++) {
        auto rand_weight = rand() % 100;
        total += (uint) rand_weight;
        be.push_back(std::shared_ptr<backend>(new backend(std::to_string(i), rand_weight, false)));
    }
    std::shared_ptr<lb> load(new lb(be, balancing_method::SWRR));
    std::vector<uint> res(be_num, 0);
    for (uint i = 0; i < total; i++) {
        res[stoi(load->get())]++;
    }
    double proc = (double) total / 100;
    for (int i = 0; i < be_num; i++) {
        if (!be[i]->down()) {
            ASSERT_TRUE(abs((double) res[i] / proc  -  (double) be[i]->weight() / proc ) < EPS_SWRR);
        }
    }
}


TEST(stress, wrr) {
    std::vector<std::shared_ptr<backend>> be;
    unsigned int total = 0;
    for (int i = 0; i < be_num; i++) {
        auto rand_weight = rand() % 100;
        total += (uint) rand_weight;
        be.push_back(std::shared_ptr<backend>(new backend(std::to_string(i), rand_weight, false)));
    }
    std::shared_ptr<lb> load(new lb(be, balancing_method::WRR));
    std::vector<uint> res(be_num, 0);
    for (uint i = 0; i < total; i++) {
        res[stoi(load->get())]++;
    }
    double proc = (double) total / 100;
    for (int i = 0; i < be_num; i++) {
        if (!be[i]->down()) {
            ASSERT_TRUE((double) res[i] / proc - (double) be[i]->weight() / proc < EPS_WRR);
        }
    }
}

TEST(stress, rand) {
    std::vector<std::shared_ptr<backend>> be;
    unsigned int total = 0;
    for (int i = 0; i < be_num; i++) {
        auto rand_weight = rand() % 100;
        total += (uint) rand_weight;
        be.push_back(std::shared_ptr<backend>(new backend(std::to_string(i), rand_weight, false)));
    }
    std::shared_ptr<lb> load(new lb(be, balancing_method::RAND));
    std::vector<uint> res(be_num, 0);
    for (uint i = 0; i < total; i++) {
        res[stoi(load->get())]++;
    }
    double proc = (double) total / 100;
    for (int i = 0; i < be_num; i++) {
        if (!be[i]->down()) {
            ASSERT_TRUE(abs((double) res[i] / proc  -  (double) be[i]->weight() / proc ) < 1);
        }
    }
}


int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
