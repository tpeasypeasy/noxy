//
// Created by master on 20.12.2019.
//

#include "lb.h"
#include <algorithm>
#include <utility>
#include "boost/random.hpp"

#include "boost/random/random_device.hpp"

std::string lb::get() {
    switch (method_) {
        case balancing_method::RR :
            return get_rr();
        case balancing_method::WRR :
            return get_wrr();
        case balancing_method::SWRR :
            return get_swrr();
        case balancing_method::RAND :
            return get_rand();
        default:
            return "";
    }
}

std::string lb::get_rr() {
    if (be_num == downs_) return "";
    for (;;) {
        std::lock_guard<std::mutex> guard(mtx);
        cur_idx++;
        if (cur_idx >= be_.size()) {
            cur_idx = 0;
        }
        if (be_.empty() || cur_idx >= be_.size()) {
            return "";
        }

        if (be_[cur_idx]->down())
            continue;
        return be_[cur_idx]->host();
    }
}

std::string lb::get_swrr() {
    if (be_num == downs_) return "";
    std::lock_guard<std::mutex> guard(mtx);

    std::shared_ptr<backend> best = nullptr;
    int total = 0;
    for (auto j : be_) { ;
        if (j->down()) {
            continue;
        }

        total += j->effective_weight();
        j->set_cur_weight(j->current_weight() + j->effective_weight());

        if (j->effective_weight() < j->weight()) {
            j->set_effective_weight(j->effective_weight() + 1);
        }

        if (best == nullptr || best->current_weight() < j->current_weight()) {
            best = j;
        }
    }
    if (best != nullptr) {
        best->set_cur_weight(best->current_weight() - total);
        return best->host();
    }
    return "";
}

std::string lb::get_wrr() {
    if (be_num == downs_) return "";
    for (int i = 0; i < be_num; i++) {
        std::lock_guard<std::mutex> guard(mtx);
        cur_idx = (cur_idx + 1) % be_num;
        if (cur_idx == 0) {
            cur_weight -= gcd;
            if (cur_weight <= 0) {
                cur_weight = max_weight;
                if (cur_weight == 0) {
                    break;
                }
            }
        }
        if (be_[cur_idx]->weight() >= cur_weight) {
            break;
        }
    }
    if (be_.empty() || cur_idx >= be_.size()) {
        return "";
    }

    if (be_[cur_idx]->down())

        return get_wrr();
    else
        return be_[cur_idx]->host();

}

std::string lb::get_rand() {
    if (be_num == downs_) return "";
    boost::random::random_device seeder;
    boost::random::mt19937 rng(seeder);
    boost::random::uniform_int_distribution<int> dist(0,be_num - 1);
    int idx = dist(rng);
    if (be_[idx]->down()) return get_rand();
    return be_[idx]->host();
}

void lb::set_helpers() {

    for (int i = 0; i < be_num - 1; i += 2) {
        if (be_[i]->weight() > max_weight) max_weight = be_[i]->weight();
        unsigned int temp = std::__gcd(be_[i]->weight(), be_[i + 1]->weight());
        gcd = std::__gcd((unsigned int) gcd, temp);
    }
}

void lb::set_down(uint inx) {
    be_[inx]->set_down();
    downs_++;
}

void lb::set_up(uint inx) {
    be_[inx]->set_up();
    downs_--;
}

lb::lb(std::vector<std::shared_ptr<backend>> be, balancing_method method) {
    be_ = std::move(be);
    method_ = method;
    be_num = be_.size();
    if (method == balancing_method::WRR) {
        set_helpers();
    }
    for (const auto& j : be_) {
        if (j->down()) downs_++;
    }
}

void lb::set_method(balancing_method method) {
    method_ = method;
    set_helpers();
}