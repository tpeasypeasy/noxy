//
// Created by master on 03.12.2019.
//

#ifndef NOXY_BACKEND_H
#define NOXY_BACKEND_H

#include <string>
#include <utility>
#include "ibackend.h"

class backend : public ibackend {
public:

    backend( unsigned int id, std::string name, std::string host,
              int weight,  bool is_down)
            : id_(id), name_(std::move(name)), host_(std::move(host)), weight_(weight),
              current_weight_(0), effective_weight_(weight), down_(is_down) {};

    backend(std::string host,
              int weight,  bool is_down)
            : id_(0), name_(""), host_(std::move(host)), weight_(weight),
              current_weight_(0), effective_weight_(weight), down_(is_down) {};


    [[nodiscard]] unsigned int id() const { return id_; }

    void set_id( unsigned int id) { id_ = id; }

    [[nodiscard]] std::string host() const override { return host_; }

    void set_host(const std::string &host) override { host_ = host; }

    [[nodiscard]] int weight() const noexcept override { return weight_; }

    void set_weight(const int weight) override { weight_ = weight; }

    [[nodiscard]] bool down() const noexcept override { return down_; }

    void set_up() override { down_ = false; }

    void set_down() override { down_ = true; }

    [[nodiscard]]  int effective_weight() const noexcept override { return effective_weight_; }

    [[nodiscard]]  int current_weight() const noexcept override { return current_weight_; }

    void set_effective_weight( int val) override { effective_weight_ = val; }

    void set_cur_weight( int val) override { current_weight_ = val; }

    ~backend() override = default;

private:
    unsigned int id_ = 0;
    std::string name_;
    std::string host_;
    int weight_ = 1;
    int current_weight_{};
    int effective_weight_{};
    bool down_ = false;

};

#endif //NOXY_BACKEND_H
