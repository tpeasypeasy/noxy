//
// Created by master on 20.12.2019.
//

#ifndef LOAD_BALANCER_LB_H
#define LOAD_BALANCER_LB_H

#include <vector>
#include "backend.h"
#include <memory>
#include <mutex>
#include <ilb.h>

enum class balancing_method {
    RR, WRR, SWRR, RAND
};

class lb : public ilb {
public:
    lb(std::vector<std::shared_ptr<backend>> be, balancing_method method);

    ~lb() override = default;

    std::string get() override;

    void set_up(uint inx) override ;

    void set_down(uint inx) override ;

    void set_method(balancing_method method);

protected:
    std::string get_rr();

    std::string get_wrr();

    std::string get_swrr();

    std::string get_rand();

    void set_helpers();

private:
    std::vector<std::shared_ptr<backend>> be_;

    balancing_method method_;

    uint cur_idx = 0;

    int gcd = 1, cur_weight = 0, max_weight = 0, be_num, downs_ = 0;

    std::mutex mtx;

};


#endif //LOAD_BALANCER_LB_H
