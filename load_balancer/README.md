# Балансировщик нагрузки

Выдает в виде строки хост, на который нужно проксировать следующий запрос

## Использование

1) инициализировать vector<std::shared_ptr<backend» указателями на бекенды
В конструктор бекенда передается "IP:Port", weight, bool isDown ( false или true)

Пример:
````
std::vector<std::shared_ptr<backend» backends = {
std::make_shared<backend>("121.167.13.228:123", 5, false),
std::make_shared<backend>("63.98.86.78:124", 1, false),
std::make_shared<backend>("228.227.147.51:98", 1, false)
};
````

Где 5, 1, 1 - веса
Булевское значение - значение isDown (недоступен ли сервер?). Если false, то доступен :)

2) Передать в конструктор lb `vector<std::shared_ptr<backend»` с бекендами и метод

Пример:
````
std::shared_ptr<lb> load(new lb(backends, RR));
````
Метод :
```
enum balancing_method {
RR, WRR, SWRR, RAND
};
```

## Методы Load Balancer

`(std::string) load->get()` - возвращает хост с портом для проксирования в виде строки

`void load->set_method(balancing_method)` - меняет текущий метод балансировки на заданный

`void set_up(uint inx)` - помечает бекенд с индексом inx из вектора бекендов как доступный

`void set_down(uint inx)` - помечает бекенд с индексом inx из вектора бекендов как недоступный
