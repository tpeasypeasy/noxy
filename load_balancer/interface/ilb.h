//
// Created by master on 21.12.2019.
//

#ifndef LOAD_BALANCER_ILB_H
#define LOAD_BALANCER_ILB_H
class ilb{
public:
    virtual ~ilb() = default;

    virtual std::string get() = 0;

    virtual void set_up(uint inx) = 0;

    virtual void set_down(uint inx) = 0;
};
#endif //LOAD_BALANCER_ILB_H
