//
// Created by master on 21.12.2019.
//

#ifndef LOAD_BALANCER_IBACKEND_H
#define LOAD_BALANCER_IBACKEND_H
class ibackend {
public:
    [[nodiscard]] virtual  std::string host() const  = 0;

    virtual void set_host(const std::string &host) = 0;

    [[nodiscard]] virtual  int weight() const noexcept = 0;

    virtual void set_weight(int weight) = 0;

    [[nodiscard]] virtual  bool down() const noexcept = 0;

    virtual void set_up() = 0;

    virtual void set_down() = 0;

    [[nodiscard]]  virtual  int effective_weight() const noexcept = 0;

    [[nodiscard]]  virtual  int current_weight() const noexcept = 0;

    virtual void set_effective_weight(int val) = 0;

    virtual void set_cur_weight(int val) = 0;

     virtual ~ibackend() = default;

};
#endif //LOAD_BALANCER_BACKEND_H
