#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <lb.h>

int main() {

    std::vector<std::shared_ptr<backend>> backends = {
            std::make_shared<backend>("a", 5, false),
            std::make_shared<backend>("b", 1, false),
            std::make_shared<backend>("c", 1, false)
    };

    std::shared_ptr<lb> load(new lb(backends, balancing_method::RR));
    std::cout << "\n\t\tRound Robin \n";

    for (int i = 0; i < 25; ++i) {
        auto myep = load->get();
        std::cout << myep << " ";
    }

    std::cout << "\n\t\tWeighted Round Robin \n";
    load->set_method(balancing_method::WRR);
    for (int i = 0; i < 25; ++i) {
        auto myep = load->get();
        std::cout << myep << " ";
    }
    std::cout << "\n\t\tSmooth Weighted Round Robin \n";
    load->set_method(balancing_method::SWRR);
    for (int i = 0; i < 25; ++i) {
        auto myep = load->get();
        std::cout << myep << " ";
    }

    std::cout << "\n\t\tRand \n";
    load->set_method(balancing_method::RAND);
    for (int i = 0; i < 25; ++i) {
        auto myep = load->get();
        std::cout << myep << " ";
    }


    return 0;
}