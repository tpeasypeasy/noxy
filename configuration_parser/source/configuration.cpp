#include "configuration.hpp"

class Configuration::PImpl {
public:
    PImpl(): lexer_cases(new LexerCase1<Word, Token>()),
             parser_handlers(new GroupParser<Token>()),
             groups(std::vector<Group>(0)),
             canditates(std::vector<Group>(0)) {}
    ~PImpl() = default;
    std::unique_ptr<LexerHandler<Word, Token>> lexer_cases;
    std::unique_ptr<ParserHandler<Token>> parser_handlers;
    std::vector<Group> groups;
    std::vector<Group> canditates;
};

Configuration::Configuration(): pimpl(new PImpl()) {}
Configuration::~Configuration() {
    delete this->pimpl;
}
void Configuration::clear() {
    this->pimpl->groups.clear();
    this->pimpl->groups.shrink_to_fit();
    this->pimpl->canditates.clear();
    this->pimpl->canditates.shrink_to_fit();
}
boost::optional<const std::vector<Group>&> Configuration::get_groups() {
    if (this->pimpl->groups.empty()) {
        return boost::none;
    } else {
        return this->pimpl->groups;
    }
}

MethodResponse Configuration::read_file(const std::string file_name, std::vector<Word>& words) {
    std::ifstream file(file_name);
    if (!file.is_open()) {
        return MethodResponse(return_code::FILE_OPENING_ERROR, "Error while opening file " + file_name);
    }
    std::string line;
    std::vector<std::string> lines;
    while (getline(file, line)) {
        lines.push_back(line);
    }
    file.close();
    std::string word;
    for (size_t i = 0; i < lines.size(); i++) {
        std::istringstream linein(lines[i]);
        while (linein >> word) {
            if (word[0] == '#') {
                break;
            }
            words.emplace_back(word, i + 1);
        }
    }
    return MethodResponse();
}

MethodResponse Configuration::lexer(const std::vector<Word>& words, std::vector<Token> &t) {
    LexerHandlerResponse<Token> hresp;
    for (auto i = words.begin(); i != words.end(); i++) {
        hresp = this->pimpl->lexer_cases->check_case(LexerHandlerRequest<Word, Token>(i, words, t));
        if (hresp.r.code != return_code::SUCCESS) {
            break;
        }
        if (hresp.token.code != token_code::EMPTY_TOKEN) {
            t.push_back(hresp.token);
        }
    }
    if (hresp.r.code != return_code::SUCCESS) {
        return hresp.r;
    }
    return MethodResponse();
}

MethodResponse Configuration::parser(const std::vector<Token>& t) {
    ParserHandlerResponse hresp;
    std::string group_name = "";
    std::vector<std::string> servers(0);
    std::vector<std::pair<std::string, int>> back(0);
    Algorithm algorithm;
    std::set<std::string> servers_ports_set;
    std::set<std::string> back_servers_set;
    for (std::vector<Token>::const_iterator i = t.begin(); i != t.end(); i++) {
        hresp = this->pimpl->parser_handlers->parse_if(ParserHandlerRequest<Token>(i, t));
        if (hresp.r.code != return_code::SUCCESS) {
            break;
        }
        if (hresp.key == "servers") {
            if (servers.empty()) {
                servers = std::vector<std::string>(hresp.v.size());
                std::copy(hresp.v.begin(), hresp.v.end(), servers.begin());
            } else {
                size_t size = servers.size();
                servers.resize(servers.size() + hresp.v.size());
                std::copy(hresp.v.begin(), hresp.v.end(), servers.begin() + size);
            }
            for (std::string s : hresp.v) {
                size_t port_separator_index = s.find_last_of(PORT_SEPARATOR);
                std::string port = s.substr(port_separator_index + 1);
                if (!servers_ports_set.insert(port).second) {
                    hresp.r = MethodResponse(return_code::PARSER_ERROR,
                                             std::to_string(i->line) +
                                             ": \x1b[0mPort " + port +
                                             " mentioned twice");
                    break;
                }
            }
            if (hresp.r.code != return_code::SUCCESS) {
                break;
            }
        }
        if (hresp.key == "back") {
            if (back.empty()) {
                back = std::vector<std::pair<std::string, int>>(hresp.vp.size());
                std::copy(hresp.vp.begin(), hresp.vp.end(), back.begin());
            } else {
                size_t size = back.size();
                back.resize(back.size() + hresp.vp.size());
                std::copy(hresp.vp.begin(), hresp.vp.end(), back.begin() + size);
            }
            for (auto s : hresp.vp) {
                if (!back_servers_set.insert(s.first).second) {
                    hresp.r = MethodResponse(return_code::PARSER_ERROR,
                                             std::to_string(i->line) +
                                             ": \x1b[0mBackend server " +
                                             s.first + " mentioned twice");
                    break;
                }
            }
            if (hresp.r.code != return_code::SUCCESS) {
                break;
            }
        }
        if (hresp.key == "algorithm") {
            bool known = false;
            for (Algorithm a : KNOWN_ALGORITHMS) {
                if (a.first == hresp.s) {
                    known = true;
                    algorithm = a;
                    break;
                }
            }
            if (!known) {
                hresp.r = MethodResponse(return_code::PARSER_ERROR,
                                         std::to_string(i->line) +
                                         ": \x1b[0mUnknown algorithm \"" +
                                         hresp.s + "\"");
            }
        }
        if (hresp.key == "name") {
            group_name = hresp.s;
        }
        if (hresp.key == "group end") {
            if (group_name != "" && algorithm.first != "" && !servers.empty() && !back.empty()) {
                if (algorithm.second) {
                    for (std::pair<std::string, int> bs : back) {
                        if (bs.second == -1) {
                            hresp.r = MethodResponse(return_code::PARSER_ERROR,
                                                     std::to_string(i->line) +
                                                     ": \x1b[0mWeights are required for " +
                                                     algorithm.first);
                            break;
                        }
                    }
                    if (hresp.r.code != return_code::SUCCESS) {
                        break;
                    }
                }
                this->pimpl->canditates.push_back(Group(group_name, servers, back, algorithm.first));
            } else {
                hresp.r = MethodResponse(return_code::PARSER_ERROR,
                                         std::to_string(i->line) +
                                         ": \x1b[0mInvalid group initialization");
                break;
            }
            group_name = "";
            algorithm = Algorithm();
            servers.clear();
            back.clear();
            servers_ports_set.clear();
            back_servers_set.clear();
        }
    }
    if (hresp.r.code != return_code::SUCCESS) {
        this->pimpl->canditates.clear();
        return hresp.r;
    }
    if (group_name != "" || algorithm.first != "" || !servers.empty() || !back.empty()) {
        this->pimpl->canditates.clear();
        return MethodResponse(return_code::PARSER_ERROR,
                              std::to_string((t.end() - 1)->line) +
                              ": \x1b[0mА где скобочка закрывающая, м?");
    }
    return MethodResponse();
}

MethodResponse Configuration::is_valid(const std::string file_name) {
    std::vector<Word> words;
    MethodResponse r = this->read_file(file_name, words);
    if (r.code != return_code::SUCCESS) {
        r.message = "\x1b[1m\x1b[91m" +
                    file_name.substr(file_name.find_last_of("/") + 1) +
                    ":" + r.message;
        return r;
    }
    std::vector<Token> t;
    r = this->lexer(words, t);
    if (r.code != return_code::SUCCESS) {
        r.message = "\x1b[1m\x1b[91m" +
                    file_name.substr(file_name.find_last_of("/") + 1) +
                    ":" + r.message;
        return r;
    }
    r = this->parser(t);
    if (r.code != return_code::SUCCESS) {
        r.message = "\x1b[1m\x1b[91m" +
                    file_name.substr(file_name.find_last_of("/") + 1) +
                    ":" + r.message;
        return r;
    }
    return MethodResponse();
}

MethodResponse Configuration::Set(const std::string file_name) {
    MethodResponse r = this->is_valid(file_name);
    if (r.code != return_code::SUCCESS) {
        return r;
    }
    this->pimpl->groups = this->pimpl->canditates;
    this->pimpl->canditates.clear();
    return MethodResponse();
}

MethodResponse Configuration::Test(const std::string file_name) {
    MethodResponse r = this->is_valid(file_name);
    this->pimpl->canditates.clear();
    if (r.code != return_code::SUCCESS) {
        return r;
    }
    return MethodResponse();
}
