#include "servers_parser.hpp"
#include "algorithm_parser.hpp"

bool numeral(char c) {
    return c >= FIRST_NUMBER && c <= LAST_NUMBER;
}
bool uppercase_letter(char c) {
    return c >= FIRST_CAPITAL_LETTER && c <= LAST_CAPITAL_LETTER;
}
bool lowercase_letter(char c) {
    return c >= FIRST_LOWERCASE_LETTER && c <= LAST_LOWERCASE_LETTER;
}

template<typename T>
class ServersParser<T>::IPImpl {
public:
    IPImpl() {}
    ~IPImpl() = default;
    MethodResponse validate_ip(std::string ip);
};

template<typename IPVersionPolicy>
class IPParser: private IPVersionPolicy {
public:
    IPParser() {}
    MethodResponse parse_ip(std::string ip, size_t port_separator_index);
    std::string set0s(const std::string& ip, size_t port_separator_index);
private:
    using IPVersionPolicy::starter;
    using IPVersionPolicy::finisher;
    using IPVersionPolicy::separator;
    using IPVersionPolicy::byte_max;
    using IPVersionPolicy::ip_size;
    using IPVersionPolicy::radix;
    using IPVersionPolicy::out_of_range_error_message;
    using IPVersionPolicy::byte_num_error_message;
};

class IPVersionPolicy4 {
protected:
    char starter() const { return '\0'; }
    char finisher() const { return '\0'; }
    char separator() const { return IPV4_SEPARATOR; }
    int byte_max() const { return IPV4_BYTE_MAX_VALUE; }
    int ip_size() const { return IPV4_SIZE; }
    int radix() const { return DEC_RADIX; }
    std::string out_of_range_error_message() const {
        return "Bytes of IP-adress (IPv4) should be in range between 0 and 255";
    }
    std::string byte_num_error_message() const {
        return "IP-adress (IPv4) should have exactly " + std::to_string(IPV4_SIZE) + " bytes";
    }
};

class IPVersionPolicy6 {
protected:
    char starter() const { return IPV6_STARTER; }
    char finisher() const { return IPV6_FINISHER; }
    char separator() const { return IPV6_SEPARATOR; }
    int byte_max() const { return IPV6_BYTE_MAX_VALUE; }
    int ip_size() const { return IPV6_SIZE; }
    int radix() const { return HEX_RADIX; }
    std::string out_of_range_error_message() const {
        return "Bytes of IP-adress (IPv6) should be in range between 0 and FFFF";
    }
    std::string byte_num_error_message() const {
        return "IP-adress (IPv6) should have exactly " + std::to_string(IPV6_SIZE) + " bytes";
    }
};

template<typename IPVersionPolicy>
MethodResponse IPParser<IPVersionPolicy>::parse_ip(std::string ip, size_t port_separator_index) {
    int byte = 0;
    int byte_num = 0;
    bool out_of_range = false;
    for (size_t i = 0; i <= port_separator_index; i++) {
        if (ip[i] != separator() && i != port_separator_index &&
            ip[i] != starter() && ip[i] != finisher())
        {
            int digit = 0;
            if (numeral(ip[i])) {
                digit = ip[i] - FIRST_NUMBER;
            } else if (uppercase_letter(ip[i])) {
                digit = ip[i] - FIRST_CAPITAL_LETTER + DEC_RADIX;
            } else if (lowercase_letter(ip[i])) {
                digit = ip[i] - FIRST_LOWERCASE_LETTER + DEC_RADIX;
            }
            byte = byte * radix() + digit;
        }
        if (ip[i] == separator() || i == port_separator_index) {
            byte_num++;
            if (byte > byte_max()) {
                out_of_range = true;
                break;
            }
            if (byte == 0 && i > 0 && ip[i] != '0') {
                if (i > 0) {
                    ip = ip.substr(0, i - 1) + "0" + ip.substr(i);
                } else {
                    ip = "0" + ip;
                }
            }
            byte = 0;
        }
    }
    if (out_of_range) {
        return MethodResponse(return_code::PARSER_ERROR, out_of_range_error_message());
    } else if (byte_num != ip_size()) {
        return MethodResponse(return_code::PARSER_ERROR, byte_num_error_message());
    }
    bool port_out_of_range = false;
    int port = 0;
    for (size_t i = port_separator_index + 1; i < ip.length(); i++) {
        int digit = ip[i] - FIRST_NUMBER;
        port = port * DEC_RADIX + digit;
        if (i == ip.length() - 1) {
            if (port < PORT_MIN_VALUE || port > PORT_MAX_VALUE) {
                port_out_of_range = true;
                break;
            }
        }
    }
    if (port_out_of_range) {
        return MethodResponse(return_code::PARSER_ERROR, "Port number should be in range between " +
                                            std::to_string(PORT_MIN_VALUE) + " and " +
                                            std::to_string(PORT_MAX_VALUE));
    }
    return MethodResponse();
}

template<typename IPVersionPolicy>
std::string IPParser<IPVersionPolicy>::set0s(const std::string& ip, size_t port_separator_index) {
    std::string r = "";
    for (size_t i = 0; i < ip.length(); i++) {
        if ((((i > 0 && ip[i - 1] == separator()) || (i == 0)) &&
            ip[i] == separator() && i < port_separator_index) ||
            (i == port_separator_index && ip[i - 1] == separator()))
        {
            if (i == 0) {
                r = "0";
            } else if (ip[i - 1] != '0') {
                r += "0";
            }
        }
        r += ip[i];
    }
    return r;
}

template<typename T>
MethodResponse ServersParser<T>::IPImpl::validate_ip(std::string ip) {
    bool v4 = false;
    bool v6 = false;
    size_t v6s = 1;
    size_t v6f = 0;
    bool unexpected_symbol = false;
    size_t port_separator_index = ip.find_last_of(PORT_SEPARATOR);
    if (port_separator_index == std::string::npos) {
        return MethodResponse(return_code::PARSER_ERROR, "No port mentioned");
    }
    for (size_t i = 0; i < ip.length(); i++) {
        if ((ip[i] != PORT_SEPARATOR && ip[i] != IPV4_SEPARATOR && ip[i] != IPV6_SEPARATOR &&
             ip[i] != IPV6_STARTER && ip[i] != IPV6_FINISHER &&
             !numeral(ip[i]) &&
             !(uppercase_letter(ip[i]) && i <= CAPITAL_F) &&
             !(lowercase_letter(ip[i]) && i <= LOWERCASE_F)) ||
             (i > port_separator_index && !numeral(ip[i])))
        {
            unexpected_symbol = true;
        } else {
            if (ip[i] == IPV4_SEPARATOR) {
                v4 = true;
            }
            if (ip[i] == IPV6_STARTER || ip[i] == IPV6_FINISHER ||
                uppercase_letter(ip[i]) || lowercase_letter(ip[i]))
            {
                v6 = true;
                if (ip[i] == IPV6_STARTER) {
                    v6s = i;
                }
                if (ip[i] == IPV6_FINISHER) {
                    v6f = i;
                }
            }
            if (v4 && v6) {
                break;
            }
        }
    }
    if (unexpected_symbol) {
        return MethodResponse(return_code::PARSER_ERROR, "Unexpected symbol in IP-adress " + ip);
    }
    if (v4 && v6) {
        return MethodResponse(return_code::PARSER_ERROR, "Impossible to state version of IP-addres " + ip);
    } else if (v4) {
        return MethodResponse(std::to_string(IPV4) + std::to_string(port_separator_index));
    } else if (v6) {
        if (v6s != 0 || v6f != port_separator_index - 1) {
            return MethodResponse(return_code::PARSER_ERROR, "Invalid notation of IP-addres (IPv6) " + ip);
        } else {
            return MethodResponse(std::to_string(IPV6) + std::to_string(port_separator_index));
        }
    } else {
        return MethodResponse(return_code::PARSER_ERROR, "Impossible to state version of IP-addres " + ip);
    }
}

template<typename T>
ServersParser<T>::ServersParser(): next(new AlgorithmParser<T>()), ip(new IPImpl()) {}

template<typename T>
ParserHandlerResponse ServersParser<T>::parse_if(ParserHandlerRequest<T> r) {
    if (!(&r.v)) {
        return ParserHandlerResponse(MethodResponse(return_code::RUNTIME_ERROR, "Empty request"));
    }
    if (r.i->code == token_code::SECTION_IDENTIFICATOR_TOKEN_CODE && (r.i->content == "servers" || r.i->content == "back")) {
        return this->parse(r);
    } else if (this->next) {
        return ParserHandlerResponse(next->parse_if(r));
    } else {
        if (r.i->code == token_code::SECTION_IDENTIFICATOR_TOKEN_CODE) {
            return ParserHandlerResponse(MethodResponse(return_code::PARSER_ERROR, std::to_string(r.i->line) +
                                                                      ": \x1b[0m" + "Unexpected section " +
                                                                      r.i->content));
        } else {
            return ParserHandlerResponse(MethodResponse(return_code::PARSER_ERROR, std::to_string(r.i->line) +
                                                                      ": \x1b[0m" + "Unexpected token " +
                                                                      r.i->content));
        }
    }
}

template<typename T>
ParserHandlerResponse ServersParser<T>::parse(ParserHandlerRequest<T> r) {
    std::string field_name = r.i->content;
    ParserHandlerResponse hresp;
    MethodResponse resp(return_code::PARSER_ERROR, std::to_string(r.i->line) +
                                      ": \x1b[0m" + "No " +
                                      (field_name == "back" ? "backend " : "") +
                                      "servers mentioned");
    for (r.i++; r.i != r.v.end() && (r.i->code == token_code::SECTION_MEMBER_TOKEN_CODE ||
         r.i->code == token_code::BACK_WEIGHT_TOKEN_CODE); r.i++)
    {
        if (r.i->code == token_code::SECTION_MEMBER_TOKEN_CODE) {
            resp = this->ip->validate_ip(r.i->content);
            if (resp.code != return_code::SUCCESS) {
                resp.message = std::to_string(r.i->line) + ": \x1b[0m" + resp.message;
                break;
            }
            std::string content = r.i->content;
            size_t port_separator_index = std::stoi(resp.message.substr(1));
            if (resp.message[0] - FIRST_NUMBER == IPV4) {
                std::unique_ptr<IPParser<IPVersionPolicy4>> ip_parser_4(new IPParser<IPVersionPolicy4>());
                resp = ip_parser_4->parse_ip(r.i->content, port_separator_index);
                if (resp.code != return_code::SUCCESS) {
                    resp.message = std::to_string(r.i->line) + ": \x1b[0m" + resp.message;
                    break;
                }
                content = ip_parser_4->set0s(r.i->content, port_separator_index);
            } else if (resp.message[0] - FIRST_NUMBER == IPV6) {
                std::unique_ptr<IPParser<IPVersionPolicy6>> ip_parser_6(new IPParser<IPVersionPolicy6>());
                resp = ip_parser_6->parse_ip(r.i->content, port_separator_index);
                if (resp.code != return_code::SUCCESS) {
                    resp.message = std::to_string(r.i->line) + ": \x1b[0m" + resp.message;
                    break;
                }
                content = ip_parser_6->set0s(r.i->content, port_separator_index);
            }
            if (field_name == "servers") {
                hresp.v.push_back(content);
            } else if (field_name == "back") {
                hresp.vp.push_back(std::pair<std::string, int>(content, -1));
            }
        } else if (r.i->code == token_code::BACK_WEIGHT_TOKEN_CODE) {
            hresp.vp.back().second = std::stoi(r.i->content);
        }
    }
    if (resp.code != return_code::SUCCESS) {
        return ParserHandlerResponse(resp);
    }
    r.i--;
    hresp.key = field_name;
    return hresp;
}

template class ServersParser<Token>;
