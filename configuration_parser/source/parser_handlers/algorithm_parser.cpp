#include "algorithm_parser.hpp"

template<typename T>
AlgorithmParser<T>::AlgorithmParser(): next(nullptr) {}

template<typename T>
ParserHandlerResponse AlgorithmParser<T>::parse_if(ParserHandlerRequest<T> r) {
    if (!(&r.v)) {
        return ParserHandlerResponse(MethodResponse(return_code::RUNTIME_ERROR, "Empty request"));
    }
    if (r.i->code == token_code::SECTION_IDENTIFICATOR_TOKEN_CODE && r.i->content == "algorithm") {
        return this->parse(r);
    } else if (this->next) {
        return ParserHandlerResponse(next->parse_if(r));
    } else {
        if (r.i->code == token_code::SECTION_IDENTIFICATOR_TOKEN_CODE) {
            return ParserHandlerResponse(MethodResponse(return_code::PARSER_ERROR, std::to_string(r.i->line) +
                                                                      ": \x1b[0m" + "Unexpected section " +
                                                                      r.i->content));
        } else {
            return ParserHandlerResponse(MethodResponse(return_code::PARSER_ERROR, std::to_string(r.i->line) +
                                                                      ": \x1b[0m" + "Unexpected token " +
                                                                      r.i->content));
        }
    }
}

template<typename T>
ParserHandlerResponse AlgorithmParser<T>::parse(ParserHandlerRequest<T> r) {
    if ((r.i + 1)->code == token_code::SECTION_MEMBER_TOKEN_CODE) {
        ParserHandlerResponse hresp((r.i + 1)->content);
        hresp.key = r.i->content;
        r.i++;
        return hresp;
    } else {
        return ParserHandlerResponse(MethodResponse(return_code::PARSER_ERROR, std::to_string(r.i->line) + ": \x1b[0m" + "No algorithm mentioned"));
    }
}

template class AlgorithmParser<Token>;
