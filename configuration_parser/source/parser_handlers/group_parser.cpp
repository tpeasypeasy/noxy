#include "group_parser.hpp"
#include "servers_parser.hpp"

template<typename T>
GroupParser<T>::GroupParser(): next(new ServersParser<T>()) {}

template<typename T>
ParserHandlerResponse GroupParser<T>::parse_if(ParserHandlerRequest<T> r) {
    if (!(&r.v)) {
        return ParserHandlerResponse(MethodResponse(return_code::RUNTIME_ERROR, "Empty request"));
    }
    if (r.i->code == token_code::GROUP_NAME_TOKEN_CODE || r.i->code == token_code::GROUP_END_TOKEN_CODE) {
        return this->parse(r);
    } else if (this->next) {
        return ParserHandlerResponse(next->parse_if(r));
    } else {
        if (r.i->code == token_code::SECTION_IDENTIFICATOR_TOKEN_CODE) {
            return ParserHandlerResponse(MethodResponse(return_code::PARSER_ERROR, std::to_string(r.i->line) +
                                                                      ": \x1b[0m" + "Unexpected section " +
                                                                      r.i->content));
        } else {
            return ParserHandlerResponse(MethodResponse(return_code::PARSER_ERROR, std::to_string(r.i->line) +
                                                                      ": \x1b[0m" + "Unexpected token " +
                                                                      r.i->content));
        }
    }
}

template<typename T>
ParserHandlerResponse GroupParser<T>::parse(ParserHandlerRequest<T> r) {
    if (r.i->code == token_code::GROUP_NAME_TOKEN_CODE && (r.i + 1) != r.v.end() && (r.i + 1)->code == token_code::GROUP_START_TOKEN_CODE) {
        ParserHandlerResponse hresp(r.i->content);
        hresp.key = "name";
        r.i++;
        return hresp;
    } else if (r.i->code == token_code::GROUP_END_TOKEN_CODE) {
        ParserHandlerResponse hresp;
        hresp.key = "group end";
        return hresp;
    } else {
        return ParserHandlerResponse(MethodResponse(return_code::PARSER_ERROR, std::to_string(r.i->line) + ": \x1b[0m" + "Invalid group declaration"));
    }
}

template class GroupParser<Token>;
