#include "lexer_case_8.hpp"
#include "lexer_case_9.hpp"

template<typename S, typename T>
LexerCase8<S, T>::LexerCase8(): next(new LexerCase9<S, T>()) {}

template<typename S, typename T>
LexerHandlerResponse<T> LexerCase8<S, T>::check_case(LexerHandlerRequest<S, T> r) {
    if (!(&r.v) || !(&r.t)) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::RUNTIME_ERROR, ": \x1b[0mEmpty request"));
    }
    if ((r.i->first != LIST_MARKER) && (!r.t.empty()) && (r.t.back().code == token_code::SECTION_IDENTIFICATOR_TOKEN_CODE)) {
        return LexerHandlerResponse<T>(MethodResponse(), T(token_code::SECTION_MEMBER_TOKEN_CODE, r.i->first, r.i->second));
    } else {
        if (this->next) {
            return LexerHandlerResponse<T>(next->check_case(r));
        } else {
            return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                       ": \x1b[0mUnexpected symbol " +
                                                                       r.i->first));
        }
    }
}

template class LexerCase8<std::pair<std::string, int>, Token>;
