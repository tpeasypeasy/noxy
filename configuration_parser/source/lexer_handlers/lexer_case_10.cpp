#include "lexer_case_10.hpp"

template<typename S, typename T>
LexerCase10<S, T>::LexerCase10(): next(nullptr) {}

template<typename S, typename T>
LexerHandlerResponse<T> LexerCase10<S, T>::check_case(LexerHandlerRequest<S, T> r) {
    if (!(&r.v)) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::RUNTIME_ERROR, ": \x1b[0mEmpty request"));
    }
    if (r.i->first == LIST_MARKER || r.i->first == LIST_STARTER) {
        return LexerHandlerResponse<T>(MethodResponse(), T());
    } else {
        if (this->next) {
            return LexerHandlerResponse<T>(next->check_case(r));
        } else {
            return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                       ": \x1b[0mUnexpected symbol " +
                                                                       r.i->first));
        }
    }
}

template class LexerCase10<std::pair<std::string, int>, Token>;
