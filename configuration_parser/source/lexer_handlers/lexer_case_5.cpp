#include "lexer_case_5.hpp"
#include "lexer_case_6.hpp"

template<typename S, typename T>
LexerCase5<S, T>::LexerCase5(): next(new LexerCase6<S, T>()) {}

template<typename S, typename T>
LexerHandlerResponse<T> LexerCase5<S, T>::check_case(LexerHandlerRequest<S, T> r) {
    if (!(&r.v)) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::RUNTIME_ERROR, ": \x1b[0mEmpty request"));
    }
    if (((r.i + 1) != r.v.end()) && ((r.i + 1)->first == LIST_STARTER)) {
        r.i++;
        return LexerHandlerResponse<T>(MethodResponse(), T(token_code::SECTION_IDENTIFICATOR_TOKEN_CODE,
                                                           (r.i - 1)->first,
                                                           r.i->second));
    } else {
        if (this->next) {
            return LexerHandlerResponse<T>(next->check_case(r));
        } else {
            return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                       ": \x1b[0mUnexpected symbol " +
                                                                       r.i->first));
        }
    }
}

template class LexerCase5<std::pair<std::string, int>, Token>;
