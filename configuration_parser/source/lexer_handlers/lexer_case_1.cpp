#include "lexer_case_1.hpp"
#include "lexer_case_2.hpp"

template<typename S, typename T>
LexerCase1<S, T>::LexerCase1(): next(new LexerCase2<S, T>()) {}

template<typename S, typename T>
LexerHandlerResponse<T> LexerCase1<S, T>::check_case(LexerHandlerRequest<S, T> r) {
    if (!(&r.v) || !(&r.t)) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::RUNTIME_ERROR, ": \x1b[0mEmpty request"));
    }
    if (r.t.empty() || (r.t.end() - 1)->code == token_code::GROUP_END_TOKEN_CODE) {
        return LexerHandlerResponse<T>(MethodResponse(), T(token_code::GROUP_NAME_TOKEN_CODE, r.i->first, r.i->second));
    } else {
        if (this->next) {
            return LexerHandlerResponse<T>(next->check_case(r));
        } else {
            return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                       ": \x1b[0mUnexpected symbol " +
                                                                       r.i->first));
        }
    }
}

template class LexerCase1<std::pair<std::string, int>, Token>;
