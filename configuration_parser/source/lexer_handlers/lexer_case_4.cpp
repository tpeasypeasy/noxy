#include "lexer_case_4.hpp"
#include "lexer_case_5.hpp"

template<typename S, typename T>
LexerCase4<S, T>::LexerCase4(): next(new LexerCase5<S, T>()) {}

template<typename S, typename T>
LexerHandlerResponse<T> LexerCase4<S, T>::check_case(LexerHandlerRequest<S, T> r) {
    if (!(&r.v)) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::RUNTIME_ERROR, ": \x1b[0mEmpty request"));
    }
    if ((r.i->first)[r.i->first.size() - 1] == LIST_STARTER[0]) {
        return LexerHandlerResponse<T>(MethodResponse(), T(token_code::SECTION_IDENTIFICATOR_TOKEN_CODE,
                                                           r.i->first.substr(0, r.i->first.size() - 1),
                                                           r.i->second));
    } else {
        if (this->next) {
            return LexerHandlerResponse<T>(next->check_case(r));
        } else {
            return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                       ": \x1b[0mUnexpected symbol " +
                                                                       r.i->first));
        }
    }
}

template class LexerCase4<std::pair<std::string, int>, Token>;
