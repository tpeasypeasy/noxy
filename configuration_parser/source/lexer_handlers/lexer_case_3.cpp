#include "lexer_case_3.hpp"
#include "lexer_case_4.hpp"

template<typename S, typename T>
LexerCase3<S, T>::LexerCase3(): next(new LexerCase4<S, T>()) {}

template<typename S, typename T>
LexerHandlerResponse<T> LexerCase3<S, T>::check_case(LexerHandlerRequest<S, T> r) {
    if (!(&r.v)) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::RUNTIME_ERROR, ": \x1b[0mEmpty request"));
    }
    if (r.i->first == GROUP_FINISHER) {
        return LexerHandlerResponse<T>(MethodResponse(), T(token_code::GROUP_END_TOKEN_CODE, r.i->second));
    } else {
        if (this->next) {
            return LexerHandlerResponse<T>(next->check_case(r));
        } else {
            return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                       ": \x1b[0mUnexpected symbol " +
                                                                       r.i->first));
        }
    }
}

template class LexerCase3<std::pair<std::string, int>, Token>;
