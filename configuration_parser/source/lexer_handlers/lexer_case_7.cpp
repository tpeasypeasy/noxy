#include "lexer_case_7.hpp"
#include "lexer_case_8.hpp"

template<typename S, typename T>
LexerCase7<S, T>::LexerCase7(): next(new LexerCase8<S, T>()) {}

template<typename S, typename T>
LexerHandlerResponse<T> LexerCase7<S, T>::check_case(LexerHandlerRequest<S, T> r) {
    if (!(&r.v)) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::RUNTIME_ERROR, ": \x1b[0mEmpty request"));
    }
    if ((r.i != r.v.begin()) && (r.i->first != LIST_MARKER) && ((r.i - 1)->first == LIST_MARKER)) {
        return LexerHandlerResponse<T>(MethodResponse(), T(token_code::SECTION_MEMBER_TOKEN_CODE, r.i->first, r.i->second));
    } else {
        if (this->next) {
            return LexerHandlerResponse<T>(next->check_case(r));
        } else {
            return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                       ": \x1b[0mUnexpected symbol " +
                                                                       r.i->first));
        }
    }
}

template class LexerCase7<std::pair<std::string, int>, Token>;
