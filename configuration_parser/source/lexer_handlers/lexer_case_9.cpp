#include "lexer_case_9.hpp"
#include "lexer_case_10.hpp"

template<typename S, typename T>
LexerCase9<S, T>::LexerCase9(): next(new LexerCase10<S, T>()) {}

template<typename S, typename T>
LexerHandlerResponse<T> LexerCase9<S, T>::check_case(LexerHandlerRequest<S, T> r) {
    if (!(&r.v) || !(&r.t)) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::RUNTIME_ERROR, ": \x1b[0mEmpty request"));
    }
    try {
        std::stoi(r.i->first);
    } catch (std::invalid_argument exc) {
        if (this->next) {
            return LexerHandlerResponse<T>(next->check_case(r));
        } else {
            return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                       ": \x1b[0mUnexpected symbol " +
                                                                       r.i->first));
        }
    }
    auto j = r.t.rbegin();
    for (; j != r.t.rend(); j++) {
        if (j->code == token_code::SECTION_IDENTIFICATOR_TOKEN_CODE) {
            break;
        }
    }
    if (j == r.t.rend()) {
        return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                   ": \x1b[0mUnexpected symbol " +
                                                                   r.i->first));
    }
    if (r.t.rbegin()->code == token_code::SECTION_MEMBER_TOKEN_CODE && j->content == "back") {
        return LexerHandlerResponse<T>(MethodResponse(), T(token_code::BACK_WEIGHT_TOKEN_CODE, r.i->first, r.i->second));
    } else {
        return LexerHandlerResponse<T>(MethodResponse(return_code::LEXER_ERROR, std::to_string(r.i->second) +
                                                                   ": \x1b[0mUnexpected symbol " +
                                                                   r.i->first));
    }
}

template class LexerCase9<std::pair<std::string, int>, Token>;
