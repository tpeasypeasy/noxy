group {
    back:
        - [df87:7fa6:9a35:638e:5ab2:b1a2:d53c:f470]:80 1
        - [2502:d8f3:01c9:f264:0387:fe74:a782:ae24]:80 1
        - [5555:e490:2a30:6aaa:c345:dcfd:1899:f031]:80 1
        - [6965:963f:77de:f9ff:7d2c:8147:45f9:61f7]:80 1

    algorithm : round_robin

    back:
        -[8dba:913b:1fd4:47b8:b71b:085c:d59c:d914]:80 1
        -[fe95:0a3e:818d:854f:97c4:ce0b:e7cb:aa4d]:80 1

    servers: [8dba:913b:1fd4:47b8:b71b:085c:d59c:d914]:80
}
