#include "configuration.hpp"
#include <gtest/gtest.h>
#include <memory>
#include <string>
using namespace std;

TEST(error_test, correct_tests) {
    unique_ptr<Configuration> c (new Configuration);
    string test_files[6] = {"../tests/correct_config_1.txt", "../tests/correct_config_2.txt",
                            "../tests/correct_config_3.txt", "../tests/correct_config_4.txt",
                            "../tests/correct_config_5.txt", "../tests/correct_config_6.txt"};
    for (string i : test_files) {
        ASSERT_EQ(return_code::SUCCESS, c->Set(i).code);
        c->clear();
    }
}

TEST(error_test, file_opening_error) {
    unique_ptr<Configuration> c (new Configuration);
    ASSERT_EQ(return_code::FILE_OPENING_ERROR, c->Set("no_such_file").code);
}

TEST(error_test, lexer_errors) {
    unique_ptr<Configuration> c (new Configuration);
    ASSERT_EQ(return_code::LEXER_ERROR, c->Set("../tests/extra_symbol_config.txt").code);
    c->clear();
    ASSERT_EQ(return_code::LEXER_ERROR, c->Set("../tests/list_without_markers_config.txt").code);
}

TEST(error_test, parser_errors) {
    unique_ptr<Configuration> c (new Configuration);
    string test_files[11] = {"../tests/ipv4_of_3_bytes_config.txt", "../tests/ipv4_with_byte_>_255_config.txt",
                             "../tests/ipv4_with_letters_config.txt", "../tests/ipv6_of_4_bytes_config.txt",
                             "../tests/ipv6_with_byte_>_ffff_config.txt", "../tests/extra_section_config.txt",
                             "../tests/empty_section_config.txt", "../tests/ip_without_port_config.txt",
                             "../tests/ip_with_port_>_65536_config.txt", "../tests/no_weights_config.txt",
                             "../tests/double_port_config.txt"};
    for (string i : test_files) {
        ASSERT_EQ(return_code::PARSER_ERROR, c->Set(i).code);
        c->clear();
    }
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
