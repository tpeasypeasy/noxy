#ifndef SECTION_HANDLER
#define SECTION_HANDLER

#include <memory>
#include <string>
#include <vector>
#include "method_response.hpp"
#include "parser.hpp"

template<typename T>
struct ParserHandlerRequest {
    ParserHandlerRequest(typename std::vector<T>::const_iterator& i, const std::vector<T>& v): i(i), v(v) {}
    typename std::vector<T>::const_iterator& i;
    const std::vector<T>& v;
};

struct ParserHandlerResponse {
    ParserHandlerResponse(): r(MethodResponse()), key("") {}
    ParserHandlerResponse(std::string s): r(MethodResponse()), s(s) {}
    ParserHandlerResponse(MethodResponse r): r(r) {}
    MethodResponse r;
    std::string key;
    std::string s;
    std::vector<std::string> v;
    std::vector<std::pair<std::string, int>> vp;
};

template<typename T>
class ParserHandler {
public:
    virtual ~ParserHandler() = default;
    virtual ParserHandlerResponse parse_if(ParserHandlerRequest<T> r) = 0;
private:
    virtual ParserHandlerResponse parse(ParserHandlerRequest<T> r) = 0;
    std::unique_ptr<ParserHandler> next;
};

#endif
