#ifndef BASE_PARSER
#define BASE_PARSER

#include <string>
#include "method_response.hpp"

enum struct token_code {
    EMPTY_TOKEN,
    GROUP_NAME_TOKEN_CODE,
    GROUP_START_TOKEN_CODE,
    GROUP_END_TOKEN_CODE,
    SECTION_IDENTIFICATOR_TOKEN_CODE,
    SECTION_MEMBER_TOKEN_CODE,
    BACK_WEIGHT_TOKEN_CODE,
};

struct Token {
    Token(): code(token_code::EMPTY_TOKEN) {}
    Token(token_code code, int line): code(code), line(line) {}
    Token(token_code code, std::string content, int line): code(code), content(content), line(line) {}
    token_code code;
    std::string content;
    int line;
};

class Lexer {
private:
    virtual MethodResponse lexer(const std::vector<std::pair<std::string, int>>& words, std::vector<Token>&) = 0;
};

class Parser {
private:
    virtual MethodResponse parser(const std::vector<Token>&) = 0;
};

#endif
