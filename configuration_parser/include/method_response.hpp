#ifndef METHOD_RESPONSE
#define METHOD_RESPONSE

enum struct return_code {
    SUCCESS,
    RUNTIME_ERROR,
    FILE_OPENING_ERROR,
    LEXER_ERROR,
    PARSER_ERROR,
};

struct MethodResponse {
    MethodResponse(): code(return_code::SUCCESS), message("No errors") {}
    MethodResponse(std::string message): code(return_code::SUCCESS), message(message) {}
    MethodResponse(return_code code, std::string message): code(code), message(message) {}
    return_code code;
    std::string message;
};

#endif
