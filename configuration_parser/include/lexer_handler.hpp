#ifndef LEXER_HANDLER
#define LEXER_HANDLER

#include <memory>
#include <string>
#include <vector>
#include "method_response.hpp"
#include "parser.hpp"

const std::string LIST_MARKER = "-";
const std::string LIST_STARTER = ":";
const std::string GROUP_STARTER = "{";
const std::string GROUP_FINISHER = "}";

template<typename S, typename T>
struct LexerHandlerRequest {
    LexerHandlerRequest(typename std::vector<S>::const_iterator& i,
                        const std::vector<S>& v,
                        const std::vector<T>& t): i(i), v(v), t(t) {}
    typename std::vector<S>::const_iterator& i;
    const std::vector<S>& v;
    const std::vector<T>& t;
};

template<typename T>
struct LexerHandlerResponse {
    LexerHandlerResponse(): r(MethodResponse()) {}
    LexerHandlerResponse(MethodResponse r): r(r) {}
    LexerHandlerResponse(MethodResponse r, T t): r(r), token(t) {}
    MethodResponse r;
    T token;
};

template<typename S, typename T>
class LexerHandler {
public:
    virtual ~LexerHandler() = default;
    virtual LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r) = 0;
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
