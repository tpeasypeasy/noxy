#ifndef CONFIGURATION_PARSER
#define CONFIGURATION_PARSER

#include <algorithm>
#include "algorithm_parser.hpp"
#include <boost/optional.hpp>
#include <fstream>
#include "group.hpp"
#include "group_parser.hpp"
#include "lexer_case_1.hpp"
#include "lexer_case_2.hpp"
#include "lexer_case_3.hpp"
#include "lexer_case_4.hpp"
#include "lexer_case_5.hpp"
#include "lexer_case_6.hpp"
#include "lexer_case_7.hpp"
#include "lexer_case_8.hpp"
#include "lexer_case_9.hpp"
#include "lexer_case_10.hpp"
#include <memory>
#include "method_response.hpp"
#include "parser.hpp"
#include "parser_handler.hpp"
#include "servers_parser.hpp"
#include <set>
#include <sstream>
#include <string>
#include <vector>

typedef std::pair<std::string, int> Word;
typedef std::pair<std::string, bool> Algorithm;

const std::vector<Algorithm> KNOWN_ALGORITHMS = {
    Algorithm("round_robin", false),
    Algorithm("weighted_round_robin", true),
    Algorithm("smooth_round_robin", true),
    Algorithm("random", false)
};

class Configuration : private Parser, Lexer {
public:
    Configuration();
    ~Configuration();
    MethodResponse Set(const std::string file_name);
    MethodResponse Test(const std::string file_name);
    void clear();
    boost::optional<const std::vector<Group>&> get_groups();
private:
    MethodResponse read_file(const std::string file_name, std::vector<Word>& words);
    MethodResponse lexer(const std::vector<Word>& words, std::vector<Token>& t);
    MethodResponse parser(const std::vector<Token>& t);
    MethodResponse is_valid(const std::string file_name);
    class PImpl;
    PImpl* pimpl;
};

#endif
