#ifndef GROUP
#define GROUP

#include <vector>

class Group {
public:
    Group() {}
    Group(std::string n, std::vector<std::string> s,
          std::vector<std::pair<std::string, int>> b, std::string a):
         name(n), servers(s), back(b), algorithm(a) {}
    const std::string& get_name() const {
        return this->name;
    }
    const std::vector<std::string>& get_servers() const {
        return this->servers;
    }
    const std::vector<std::pair<std::string, int>>& get_back() const {
        return this->back;
    }
    const std::string& get_algorithm() const {
        return this->algorithm;
    }
private:
    std::string name;
    std::vector<std::string> servers;
    std::vector<std::pair<std::string, int>> back;
    std::string algorithm;
};

#endif
