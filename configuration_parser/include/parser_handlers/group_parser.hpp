#ifndef GROUP_PARSER
#define GROUP_PARSER

#include "parser_handler.hpp"

template<typename T>
class GroupParser : public ParserHandler<T> {
public:
    GroupParser();
    ~GroupParser() = default;
    ParserHandlerResponse parse_if(ParserHandlerRequest<T> r);
private:
    ParserHandlerResponse parse(ParserHandlerRequest<T> r);
    std::unique_ptr<ParserHandler<T>> next;
};

#endif
