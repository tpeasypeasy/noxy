#ifndef SERVERS_PARSER
#define SERVERS_PARSER

#include "parser_handler.hpp"

const char FIRST_NUMBER = '0';
const char LAST_NUMBER = '9';
const char FIRST_CAPITAL_LETTER = 'A';
const char LAST_CAPITAL_LETTER = 'Z';
const char CAPITAL_F = 'F';
const char FIRST_LOWERCASE_LETTER = 'a';
const char LAST_LOWERCASE_LETTER = 'z';
const char LOWERCASE_F = 'f';

const int DEC_RADIX = 10;
const int HEX_RADIX = 16;
const int IPV4 = 4;
const int IPV4_SIZE = 4;
const int IPV4_BYTE_MAX_VALUE = 255;
const char IPV4_SEPARATOR = '.';
const char IPV6_STARTER = '[';
const char IPV6_FINISHER = ']';
const int IPV6 = 6;
const int IPV6_SIZE = 8;
const int IPV6_BYTE_MAX_VALUE = 65535;
const char IPV6_SEPARATOR = ':';

const char PORT_SEPARATOR = ':';
const int PORT_MIN_VALUE = 0;
const int PORT_MAX_VALUE = 65536;

template<typename T>
class ServersParser : public ParserHandler<T> {
public:
    ServersParser();
    ~ServersParser() = default;
    ParserHandlerResponse parse_if(ParserHandlerRequest<T> r);
private:
    ParserHandlerResponse parse(ParserHandlerRequest<T> r);
    std::unique_ptr<ParserHandler<T>> next;
    class IPImpl;
    std::unique_ptr<IPImpl> ip;
};

#endif
