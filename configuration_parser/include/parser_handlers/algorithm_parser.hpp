#ifndef ALGORITHM_PARSER
#define ALGORITHM_PARSER

#include "parser_handler.hpp"

template<typename T>
class AlgorithmParser : public ParserHandler<T> {
public:
    AlgorithmParser();
    ~AlgorithmParser() = default;
    ParserHandlerResponse parse_if(ParserHandlerRequest<T> r);
private:
    ParserHandlerResponse parse(ParserHandlerRequest<T> r);
    std::unique_ptr<ParserHandler<T>> next;
};

#endif
