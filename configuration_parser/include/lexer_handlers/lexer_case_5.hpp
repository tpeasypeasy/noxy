#ifndef LEXER_CASE_5
#define LEXER_CASE_5

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase5 : public LexerHandler<S, T> {
public:
    LexerCase5();
    ~LexerCase5() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
