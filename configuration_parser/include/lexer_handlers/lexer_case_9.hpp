#ifndef LEXER_CASE_9
#define LEXER_CASE_9

#include "lexer_handler.hpp"
#include <stdexcept>

template<typename S, typename T>
class LexerCase9 : public LexerHandler<S, T> {
public:
    LexerCase9();
    ~LexerCase9() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
