#ifndef LEXER_CASE_4
#define LEXER_CASE_4

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase4 : public LexerHandler<S, T> {
public:
    LexerCase4();
    ~LexerCase4() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
