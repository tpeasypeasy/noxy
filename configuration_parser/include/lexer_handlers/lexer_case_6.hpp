#ifndef LEXER_CASE_6
#define LEXER_CASE_6

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase6 : public LexerHandler<S, T> {
public:
    LexerCase6();
    ~LexerCase6() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
