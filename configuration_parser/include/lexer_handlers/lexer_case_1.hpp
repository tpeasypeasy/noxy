#ifndef LEXER_CASE_1
#define LEXER_CASE_1

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase1 : public LexerHandler<S, T> {
public:
    LexerCase1();
    ~LexerCase1() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
