#ifndef LEXER_CASE_7
#define LEXER_CASE_7

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase7 : public LexerHandler<S, T> {
public:
    LexerCase7();
    ~LexerCase7() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
