#ifndef LEXER_CASE_2
#define LEXER_CASE_2

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase2 : public LexerHandler<S, T> {
public:
    LexerCase2();
    ~LexerCase2() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
