#ifndef LEXER_CASE_8
#define LEXER_CASE_8

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase8 : public LexerHandler<S, T> {
public:
    LexerCase8();
    ~LexerCase8() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
