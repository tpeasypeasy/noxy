#ifndef LEXER_CASE_10
#define LEXER_CASE_10

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase10 : public LexerHandler<S, T> {
public:
    LexerCase10();
    ~LexerCase10() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
