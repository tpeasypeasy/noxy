#ifndef LEXER_CASE_3
#define LEXER_CASE_3

#include "lexer_handler.hpp"

template<typename S, typename T>
class LexerCase3 : public LexerHandler<S, T> {
public:
    LexerCase3();
    ~LexerCase3() = default;
    LexerHandlerResponse<T> check_case(LexerHandlerRequest<S, T> r);
private:
    std::unique_ptr<LexerHandler<S, T>> next;
};

#endif
