#include "configuration.hpp"
#include <iostream>
#include <memory>
using namespace std;

void print_results(unique_ptr<Configuration>& c) {
    //checking if there are any groups, using boost::optional
    if (c->get_groups()) {
        for (Group i : *c->get_groups()) {
            //using methods of Group to get data from configuration
            cout << i.get_name() << ":\n";
            cout << "  servers:\n";
            for (auto j : i.get_servers()) {
                cout << "    " << j << "\n";
            }
            cout << "  back servers:\n";
            for (auto j : i.get_back()) {
                /* -1 is a scecial value for weight meaning that there hadn't been
                   mentioned any weight, so there is no need to print it */
                if (j.second != -1) {
                    cout << "    " << j.first << " " << j.second << "\n";
                } else {
                    cout << "    " << j.first << "\n";
                }
            }
            cout << "  algorithm: " << i.get_algorithm() << endl << endl;
        }
    }
}

void print_results(MethodResponse r, unique_ptr<Configuration>& c) {
    if (r.code == return_code::SUCCESS) {
        cout << "\x1b[95m\x1b[1m" << "current configuration:" << "\x1b[0m\n";
        //checking if there are any groups, using boost::optional
        if (c->get_groups()) {
            for (Group i : *c->get_groups()) {
                //using methods of Group to get data from configuration
                cout << i.get_name() << ":\n";
                cout << "  servers:\n";
                for (auto j : i.get_servers()) {
                    cout << "    " << j << "\n";
                }
                cout << "  back servers:\n";
                for (auto j : i.get_back()) {
                    /* -1 is a scecial value for weight meaning that there hadn't been
                       mentioned any weight, so there is no need to print it */
                    if (j.second != -1) {
                        cout << "    " << j.first << " " << j.second << "\n";
                    } else {
                        cout << "    " << j.first << "\n";
                    }
                }
                cout << "  algorithm: " << i.get_algorithm() << endl << endl;
            }
        }
    } else {
        cout << r.message << endl;
    }
}

int main(int argc, char** argv) {
    unique_ptr<Configuration> c (new Configuration);
    string file;
    /* reading path to file from command line arguments
       (this one is supposed to be correct so that the example demonstrates the whole set of possibilities)
       e.g. ../example/config.txt */
    if (argc >= 2) file = argv[1];
    else return 0;

    cout << "\x1b[95m\x1b[1m" << "setting configuration from " << file << " ..." << "\x1b[0m\n";

    //setting new configuration
    MethodResponse r = c->Set(file);
    print_results(r, c); //will print either error message, or the whole current configuration
    /* reading path to file from command line arguments
       (this one is supposed to be incorrect so that the example demonstrates the whole set of possibilities)
       e.g. any of ../tests/ */
    if (argc >= 3) file = argv[2];
    else return 0;

    cout << "\x1b[95m\x1b[1m" << "testing configuration from " << file << " ..." << "\x1b[0m\n";

    //testing new configuration
    r = c->Test(file);

    cout << "\x1b[95m\x1b[1m" << "configuration in " << file << " is " << (r.code == return_code::SUCCESS ? "" : "in") << "valid" << "\x1b[0m\n";

    print_results(r, c); /*will print either error message, or the whole current configuration,
                           so you can make shure it wasn't affected */

    //if previous line printed only error message, this piece will print the whole current configuration
    if (r.code != return_code::SUCCESS) {
        cout << "\x1b[95m\x1b[1m" << "current configuration:" << "\x1b[0m\n";
        print_results(c);
    }
    cout << "\x1b[95m\x1b[1m" << "setting configuration from " << file << " ..." << "\x1b[0m\n";

    //setting new configuration from just tested file anyway
    r = c->Set(file);
    print_results(r, c); /*will print either error message, or the whole current configuration,
                           so you can make shure that this time it was affected */

    //if previous line printed only error message, this piece will print the whole current configuration
    if (r.code != return_code::SUCCESS) {
        cout << "\x1b[95m\x1b[1m" << "current configuration:" << "\x1b[0m\n";
        print_results(c);
    }
    return 0;
}
