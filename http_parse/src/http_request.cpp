//
// Created by nickeskov on 05.12.2019.
//

#include <string>
#include <string_view>
#include <type_traits>

#include "http_request.h"

namespace {
    constexpr std::size_t max_headers_length = 16u * 1024u;
    constexpr std::string_view headers_separator = std::string_view("\r\n\r\n");
}

HttpRequest::HttpRequest() {
    static_assert(std::is_same<http_data_type, std::string>(),
            "http_data_type must be string");
}

const HttpHeaders &HttpRequest::get_headers() const noexcept {
    return _headers;
}

bool HttpRequest::append(HttpRequest::http_data_type &data) {
    if (!_headers_ready) {
        if (!append_headers(data)) {
            _headers_ready = false;
            _request_valid = false;
            return false;
        }
        _content_length_needed = _headers.get_content_len();
    }

    if (_headers.get_header("Transfer-Encoding") == "chunked") {
        // TODO(nickeskov): must throw exception, because this part of libraty now not implemented
        _request_valid = true; // TODO(nickeskov): hack!
        return true;
    }

    if (_content_length_needed) {
        const auto body_part = data.substr(0, _content_length_needed);
        _body += body_part;
        _content_length_needed -= body_part.length();
        return true;
    }

    _request_valid = true;
    return true;
}

bool HttpRequest::is_valid() const noexcept {
    return _request_valid;
}

void HttpRequest::clear() {
    _headers.clear();

    _body = http_data_type();
    _headers_string = http_headers_data_type();

    _headers_ready = false;
    _request_valid = false;
    _chunked = false;
    _chunk_size = 0;
}

bool HttpRequest::append_headers(HttpRequest::http_data_type &str) {
    if (_headers_ready) {
        return true;
    }

    if (_headers_string.length() > max_headers_length) {
        _headers_string = "";
//        _headers_ready = false;
//        _request_valid = false;
        // TODO(nickeskov): must throw exception, err_code=413, message='Entity Too Large'
        return false;
    }

    size_t pos = _headers_string.find(headers_separator);
    if (pos == std::string::npos) {
        _headers_string += str;
        str = "";
        return true;
    }
    _headers_string += str.substr(0, pos);
    str = str.substr(pos + headers_separator.length());
    _headers.from(_headers_string);
    _headers_ready = true;
    return true;
}

std::string HttpRequest::to_string() const {
    return _headers.to_string() + _body;
}
