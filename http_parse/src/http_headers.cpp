//
// Created by nickeskov on 05.12.2019.
//

#include <iostream>
#include <sstream>
#include <vector>

#include <unistd.h>

#include "http_parse_utils.h"
#include "http_headers.h"

void HttpHeaders::from(const HttpHeaders::data_type &str) {
    std::vector<std::string> hdrs = utils::split(str, "\r\n");

    parse_first_request_line(hdrs[0]);

    for (size_t i = 1; i < hdrs.size(); i++) {
        if (hdrs[i].empty())
            continue;

        if (::isspace(hdrs[i][0]))
            continue;

        std::vector<std::string> key_value = utils::split(hdrs[i], ":", 2);
        if (key_value.size() < 2)
            continue;

        std::string key = utils::trimmed(key_value[0]);

        _headers[key] = utils::trimmed(key_value[1]);
    }

    _content_len = 0;

    std::string content_len = get_header("Content-Length");
    if (content_len.empty()) {
        return;
    }

    size_t c_len = std::stoi(content_len);
    if (c_len == 0) {
        return;
    }

    _content_len = c_len;
}

std::string HttpHeaders::to_string() const {
    std::stringstream ss;

    ss << _method << " " << _resource << " " << _version;

    if (!_headers.empty()) {
        ss << "\r\n";
    }

    size_t counter = 0;
    for (const auto & _header : _headers) {
        ss << _header.first << ": " << _header.second;
        ++counter;
        if (counter < _headers.size()) {
            ss << "\r\n";
        }
    }

    return ss.str();
}

void HttpHeaders::parse_first_request_line(const HttpHeaders::data_type &str) {
    std::vector<std::string> parts = utils::split(str, " ");
    if (parts.size() < 3) {
        std::cerr << "parse_first_request_line: broken header" << std::endl;
        return;
    }

    _method = parts[0];
    _version = parts[parts.size() - 1];

    for (size_t i = 1; i < parts.size() - 1; ++i) {
        if (i > 1) {
            _resource += " ";
        }
        _resource += parts[i];
    }
}

bool HttpHeaders::has_header(const HttpHeaders::data_type &hdr) const {
    return _headers.find(hdr) != _headers.end();
}

HttpHeaders::data_type HttpHeaders::get_header(const HttpHeaders::data_type &hdr) const {
    const auto it = _headers.find(hdr);
    if (it != _headers.end()) {
        return it->second;
    }
    return data_type();
}

const HttpHeaders::data_type &HttpHeaders::get_method() const noexcept {
    return _method;
}

const HttpHeaders::data_type &HttpHeaders::get_resource() const noexcept {
    return _resource;
}

const HttpHeaders::data_type &HttpHeaders::get_version() const noexcept {
    return _version;
}

HttpHeaders::size_type HttpHeaders::get_content_len() const noexcept {
    return _content_len;
}

void HttpHeaders::clear() noexcept {
    _headers.clear();
    _method = data_type();
    _resource = data_type();
    _version = data_type();
    _content_len = size_type();
}
