//
// Created by nickeskov on 07.12.2019.
//

#include <type_traits>
#include <string>

#include "http_response.h"

HttpResponse::HttpResponse() {
    static_assert(std::is_same<http_data_type, std::string>(),
                  "http_data_type must be string");
}

const HttpHeaders &HttpResponse::get_headers() const noexcept {
    return _rq.get_headers();
}

bool HttpResponse::append(http_data_type &data) {
    return _rq.append(data);
}

bool HttpResponse::is_valid() const noexcept {
    return _rq.is_valid();
}

void HttpResponse::clear() {
    _rq.clear();
}

std::string HttpResponse::to_string() const {
    return _rq.to_string();
}
