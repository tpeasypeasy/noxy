//
// Created by nickeskov on 05.12.2019.
//

#ifndef HTTP_PARSE_HTTP_HEADERS_H
#define HTTP_PARSE_HTTP_HEADERS_H

#include <string>
#include <map>

#include "ihttp_headers.h"


struct HttpHeaders : public IHttpHeaders<std::string> {
  public:

    void from(const data_type &str) override;
    [[nodiscard]] std::string to_string() const override;

    void parse_first_request_line(const data_type &str) override;

    [[nodiscard]] bool has_header(const data_type &hdr) const override;

    [[nodiscard]] data_type get_header(const data_type &hdr) const override;
    [[nodiscard]] const data_type &get_method() const noexcept override;
    [[nodiscard]] const data_type &get_resource() const noexcept override;
    [[nodiscard]] const data_type &get_version() const noexcept override;
    [[nodiscard]] size_type get_content_len() const noexcept override;

    void clear() noexcept override;

  private:
    std::map<data_type , data_type> _headers;
    data_type _method;
    data_type _resource;
    data_type _version;

    size_type _content_len;
};

#endif //HTTP_PARSE_HTTP_HEADERS_H
