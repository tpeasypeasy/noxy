//
// Created by nickeskov on 05.12.2019.
//


#ifndef HTTP_PARSE_HTTP_REQUEST_H
#define HTTP_PARSE_HTTP_REQUEST_H

#include "ihttp_request.h"
#include "http_headers.h"

struct HttpRequest : public IHttpRequest<HttpHeaders> {
  public:
    using http_data_type = typename IHttpRequest<HttpHeaders>::http_headers_data_type;

    HttpRequest();

    [[nodiscard]] const HttpHeaders &get_headers() const noexcept override;
    bool append(http_data_type &data) override;
    [[nodiscard]] bool is_valid() const noexcept override ;
    void clear() override;

    [[nodiscard]] std::string to_string() const override;

private:
    HttpHeaders _headers;
    http_data_type _body;
    http_data_type _headers_string;

    bool _request_valid = false;
    bool _chunked = false;
    bool _headers_ready = false;
    std::size_t _chunk_size = 0;
    std::size_t _content_length_needed = 0;

    bool append_headers(http_data_type &str);

    // void process_chunked(const http_headers_data_type)
};

#endif //HTTP_PARSE_HTTP_REQUEST_H
