//
// Created by nickeskov on 05.12.2019.
//

#ifndef HTTP_PARSE_HTTP_PARSE_UTILS_H
#define HTTP_PARSE_HTTP_PARSE_UTILS_H

#include <vector>
#include <string>


namespace utils {
    std::vector<std::string> split(const std::string &line, const std::string &delimiter);
    std::vector<std::string> split(const std::string &line, const std::string &delimiter, int max_parts);

    bool starts_with(const std::string &s, const std::string &predicate);
    bool ends_with(const std::string &s, const std::string &predicate);

    std::string lowercased(const std::string &str);
    std::string trimmed(std::string s);
}

#endif //HTTP_PARSE_HTTP_PARSE_UTILS_H
