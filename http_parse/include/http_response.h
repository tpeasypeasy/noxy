//
// Created by nickeskov on 07.12.2019.
//

#ifndef HTTP_PARSE_HTTP_RESPONSE_H
#define HTTP_PARSE_HTTP_RESPONSE_H

#include "ihttp_response.h"
#include "http_request.h"
#include "http_headers.h"

class HttpResponse : public IHttpResponse<HttpHeaders> {
  public:
    using http_data_type = typename IHttpResponse<HttpHeaders>::http_headers_data_type;

    HttpResponse();

    [[nodiscard]] const HttpHeaders &get_headers() const noexcept override;
    bool append(http_data_type &data) override;
    [[nodiscard]] bool is_valid() const noexcept override ;
    void clear() override;

    [[nodiscard]] std::string to_string() const override;

  private:
    HttpRequest _rq;
};


#endif //HTTP_PARSE_HTTP_RESPONSE_H
