//
// Created by nickeskov on 05.12.2019.
//

#ifndef HTTP_PARSE_IHTTP_RESPONSE_H
#define HTTP_PARSE_IHTTP_RESPONSE_H

#include <type_traits>
#include <string>

#include "ihttp_headers.h"


template <typename http_headers_type>
struct IHttpResponse {
  public:
    using http_headers_data_type = typename http_headers_type::data_type;

    IHttpResponse() {
        static_assert(std::is_base_of<IHttpHeaders<http_headers_data_type>, http_headers_type>::value,
                "Your http_headers_type does not implement http_headers interface.");
    }

    virtual const http_headers_type& get_headers() const = 0;

    virtual bool append(http_headers_data_type &data) = 0;
    [[nodiscard]] virtual bool is_valid() const = 0;
    virtual void clear() = 0;

    [[nodiscard]] virtual std::string to_string() const = 0;

    virtual ~IHttpResponse() = default;
};

#endif //HTTP_PARSE_IHTTP_RESPONSE_H
