//
// Created by nickeskov on 04.12.2019.
//

#ifndef HTTP_PARSE_IHTTP_HEADERS_H
#define HTTP_PARSE_IHTTP_HEADERS_H

#include <string>


template <typename data>
struct IHttpHeaders {
  public:
    using data_type = data;
    using size_type = std::size_t;

    virtual void from(const data_type &str) = 0;
    [[nodiscard]] virtual std::string to_string() const = 0;

    virtual void parse_first_request_line(const data_type &str) = 0;

    virtual bool has_header(const data_type &hdr) const = 0;

    virtual data_type get_header(const data_type &hdr) const = 0;
    virtual const data_type& get_method() const = 0;
    virtual const data_type& get_resource() const = 0;
    virtual const data_type& get_version() const = 0;
    [[nodiscard]] virtual size_type get_content_len() const = 0;

    virtual void clear() = 0;

    virtual ~IHttpHeaders() = default;
};

#endif //HTTP_PARSE_IHTTP_HEADERS_H
