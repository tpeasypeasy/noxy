//
// Created by nickeskov on 27.12.2019.
//

#ifndef NOXY_GROUP_SERVER_H
#define NOXY_GROUP_SERVER_H

#include "group.hpp"
#include "ilogger.h"
#include "iserver.h"
#include "epoll_engine.h"

#include <vector>
#include <memory>
#include <future>

class GroupServer : public IServer {
  public:

    GroupServer(Group &group, const std::shared_ptr<ILogger> &logger);

    GroupServer(const GroupServer &other) = delete;

    GroupServer &operator=(const GroupServer &other) = delete;

    GroupServer(GroupServer &&other) noexcept;

    GroupServer &operator=(GroupServer &&other) noexcept;

    void run() override;

    void stop() override;

    ~GroupServer() override;

  private:
    struct Impl;
    std::unique_ptr<Impl> _impl;
};

#endif //NOXY_GROUP_SERVER_H
