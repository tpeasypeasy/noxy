//
// Created by nickeskov on 26.12.2019.
//

#ifndef NOXY_NOXY_UTILS_H
#define NOXY_NOXY_UTILS_H

#include "host_holder.h"

#include <memory>

namespace utils {
    HostHolder to_host_holder(const std::string &str);
}

#endif //NOXY_NOXY_UTILS_H
