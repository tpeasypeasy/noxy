//
// Created by nickeskov on 27.12.2019.
//

#ifndef NOXY_BOOST_TRIVIAL_LOGGER_H
#define NOXY_BOOST_TRIVIAL_LOGGER_H

#include "ilogger.h"

class BoostTrivialLogger : public ILogger {
  public:
    void trace(const std::string &str) override;

    void debug(const std::string &str) override;

    void info(const std::string &str) override;

    void warning(const std::string &str) override;

    void error(const std::string &str) override;

    void fatal(const std::string &str) override;

    ~BoostTrivialLogger() noexcept override = default;
};

#endif //NOXY_BOOST_TRIVIAL_LOGGER_H
