//
// Created by nickeskov on 26.12.2019.
//

#ifndef NOXY_BACKENDS_DISPENSER_H
#define NOXY_BACKENDS_DISPENSER_H

#include "idispenser.h"
#include "ihost_holder.h"
#include "ilb.h"

#include <memory>
#include <mutex>

class BackendsDispenser : public IDispenser<std::unique_ptr<IHostHolder>> {
  public:

    explicit BackendsDispenser(std::shared_ptr<ilb> load_balancer);

    void set_balancer(std::shared_ptr<ilb> load_balancer);

    return_type dispense() override;

  private:

    std::shared_ptr<ilb> _load_balancer;
    std::mutex _mutex;
};


#endif //NOXY_BACKENDS_DISPENSER_H
