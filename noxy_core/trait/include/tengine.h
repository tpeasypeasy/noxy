//
// Created by nickeskov on 07.12.2019.
//

#ifndef NOXY_CORE_TENGINE_H
#define NOXY_CORE_TENGINE_H


#include <sys/socket.h> // socket()
#include <netinet/in.h> // htons(), INADDR_ANY
#include <cstring>     // memset(), strerror()
#include <unistd.h>     // close()
#include <cerrno>

#include <stdexcept>
#include <string>
#include <atomic>
#include <cinttypes>

#include <memory>
#include <iostream>
#include <utility>
#include <mutex>
#include <string>
#include <arpa/inet.h>

#include "noxy_core_utils.h"
#include "iengine.h"
#include "proxy_client.h"
#include "idispenser.h"
#include "ihost_holder.h"
#include "ilogger.h"

template <typename event_t>
class TEngine : public IEngine<std::string, int, event_t> {
  public:
    using host_dispenser_type = IDispenser<std::unique_ptr<IHostHolder>>;
    using tclient_type = typename IEngine<std::string, int, event_t>::iclient_type;

    TEngine(const IHostHolder &this_host, std::shared_ptr<host_dispenser_type> dispenser,
            std::shared_ptr<ILogger> logger);

    virtual void change_dispenser(const std::shared_ptr<host_dispenser_type>& new_dispenser);

    void stop() override;

    [[nodiscard]] bool is_stopped() const noexcept override;

    void run() override = 0;

    [[nodiscard]] bool add_to_event_loop(tclient_type *client, iengine::event_t event) override = 0;

    [[nodiscard]] bool change_event(tclient_type *client, iengine::event_t event) override = 0;

    ~TEngine() override;

  protected:

    [[nodiscard]] int get_listener() const;
    [[nodiscard]] host_dispenser_type::return_type get_host() const;
    void reset_stop() noexcept;

    std::shared_ptr<host_dispenser_type> _host_dispenser;
    std::shared_ptr<ILogger> _logger;
    mutable std::mutex _dispenser_lock;

  private:

    std::atomic<bool> _stop;
    int _listener;

    int create_listen_socket(const IHostHolder &this_host, uint32_t backlog_queue_size);
};

template<typename event_t>
TEngine<event_t>::TEngine(const IHostHolder &this_host, std::shared_ptr<host_dispenser_type> dispenser,
        std::shared_ptr<ILogger> logger)
    : _host_dispenser(std::move(dispenser))
    , _logger(std::move(logger))
    , _dispenser_lock()
    ,_stop(false)
    , _listener(create_listen_socket(this_host, SOMAXCONN)) {

    if (_listener == -1) {
        std::string what = utils::to_string("error listen socket [port: "
                , this_host.get_port(), ", error_description: ", strerror(errno), ']');
        throw std::runtime_error(what);
    }
}

template<typename event_t>
void TEngine<event_t>::stop() {
    _stop = true;
}

template<typename event_t>
TEngine<event_t>::~TEngine() {
    if (close(_listener) == -1) {
        _logger->error(utils::to_string("Error wile close listener socket: ", strerror(errno)));
    }
}

template<typename event_t>
int TEngine<event_t>::get_listener() const {
    return _listener;
}

template<typename event_t>
int TEngine<event_t>::create_listen_socket(const IHostHolder &this_host, uint32_t backlog_queue_size) {
    int sd = -1;
    if (this_host.is_ipv6()) {
        sd = socket(PF_INET6, SOCK_STREAM, IPPROTO_TCP);
    } else {
        sd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    }

    if (sd == -1) {
        _logger->error(utils::to_string("listen socket 'creation' error: ", strerror(errno)));
        return -1;
    }

    int yes = 1;
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
        _logger->error(utils::to_string("listen socket 'set reuseaddr option' error: " , strerror(errno)));
        close(sd);
        return -1;
    }

    if (utils::set_nonblock(sd) == -1) {
        _logger->error(utils::to_string("listen socket 'set nonblock' error: " , strerror(errno)));
        close(sd);
        return -1;
    }

    struct sockaddr_storage serv_addr{};
    std::string ip = this_host.get_ip();
    int pton_status = -1;


    if (this_host.is_ipv6()) {
        auto *serv_addr_ipv6 = reinterpret_cast<struct sockaddr_in6 *>(&serv_addr);
        serv_addr_ipv6->sin6_port = htons(this_host.get_port());
        serv_addr_ipv6->sin6_family = AF_INET6;
        pton_status = inet_pton(AF_INET6, ip.data(), &(serv_addr_ipv6->sin6_addr));
    } else {
        auto *serv_addr_ipv4 = reinterpret_cast<struct sockaddr_in *>(&serv_addr);
        serv_addr_ipv4->sin_port = htons(this_host.get_port());
        serv_addr_ipv4->sin_family = AF_INET;
        pton_status = inet_pton(AF_INET, ip.data(), &(serv_addr_ipv4->sin_addr));
    }

    if (pton_status != 1) {
        if (pton_status == -1) {
            _logger->error(utils::to_string("create_partner_client fails, ip address not valid: "
                    , strerror(errno)));
        } else if (pton_status == 0) {
            _logger->error("create_partner_client fails, input string does not contain a character"
                           "string representing a valid network address in the specified address family.");
        }
        close(sd);
        return -1;
    }

    if (bind(sd, reinterpret_cast<struct sockaddr *>(&serv_addr), sizeof(serv_addr)) == -1) {
        _logger->error(utils::to_string("listen socket 'bind' error: " , strerror(errno)));
        close(sd);
        return -1;
    }

    if (listen(sd, backlog_queue_size) == -1) {
        _logger->error(utils::to_string("listen socket 'listen' error: " , strerror(errno)));
        close(sd);
        return -1;
    }

    return sd;
}

template<typename event_t>
bool TEngine<event_t>::is_stopped() const noexcept {
    return _stop;
}

template<typename event_t>
void TEngine<event_t>::change_dispenser(const std::shared_ptr<host_dispenser_type>& new_dispenser) {
    std::lock_guard<std::mutex> lock(_dispenser_lock);
    _host_dispenser = new_dispenser;
}

template<typename event_t>
std::unique_ptr<IHostHolder> TEngine<event_t>::get_host() const {
    std::lock_guard<std::mutex> lock(_dispenser_lock);
    return _host_dispenser->dispense();
}
template<typename event_t>
void TEngine<event_t>::reset_stop() noexcept {
    _stop = false;
}



#endif //NOXY_CORE_TENGINE_H
