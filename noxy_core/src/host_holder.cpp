//
// Created by nickeskov on 19.12.2019.
//

#include "host_holder.h"

#include <utility>

HostHolder::HostHolder(std::string ip, int port, bool is_ipv6)
    : _ip(std::move(ip)), _port(port), _is_ipv6(is_ipv6) { }

std::string HostHolder::get_ip() const {
    return _ip;
}

int HostHolder::get_port() const noexcept {
    return _port;
}

bool HostHolder::is_ipv6() const noexcept {
    return _is_ipv6;
}
