//
// Created by nickeskov on 07.12.2019.
//

#include <deque>
#include <utility>

#include "client_stream.h"

struct ClientStream::Impl {
  public:
    std::deque<data_type> buf;
    size_type size = 0;
};

ClientStream::ClientStream(): _impl(std::make_unique<ClientStream::Impl>()) { }

void ClientStream::append(const IAbstractStream::data_type &data) {
    _impl->size += data.length();
    _impl->buf.push_back(data);
}

void ClientStream::append(IAbstractStream::data_type &&data) {
    _impl->size += data.length();
    _impl->buf.push_back(std::move(data));
}

std::size_t ClientStream::size() const noexcept {
    return _impl->size;
}

void ClientStream::clear() {
    _impl->buf.clear();
    _impl->size = 0;
}

std::string ClientStream::get() const {
    IAbstractStream::data_type dump;
    dump.reserve(_impl->size);

    for (const auto &elem : _impl->buf) {
        dump += elem;
    }
    return dump;
}

std::string ClientStream::get_pack() {
    IAbstractStream::data_type pack;

    if (!_impl->buf.empty()) {
        pack = std::move(_impl->buf.front());
        _impl->buf.pop_front();
        _impl->size -= pack.length();
    }
    return pack;
}

bool ClientStream::empty() const noexcept {
    return _impl->size == 0;
}

ClientStream::~ClientStream() = default;
