//
// Created by nickeskov on 07.12.2019.
//

#include <iostream>
#include <utility>
#include <sys/socket.h>
#include <unistd.h>

#include "proxy_client.h"
#include "noxy_core_utils.h"


struct ProxyClient::Impl {
  public:
    Impl()
        : sd(-1)
        , state(proxy_client::state::UNKNOWN)
        , engine(nullptr)
        , partner(nullptr)
        , stream()
        , logger() { }

    Impl(IClient::io_service_type sd, ProxyClient::iengine_type *engine, std::shared_ptr<ILogger> logger)
        : sd(sd)
        , state(proxy_client::state::INIT)
        , engine(engine)
        , partner(nullptr)
        , stream()
        , logger(std::move(logger)) { }

  public:
    int sd;
    proxy_client::state state;
    iengine_type *engine;
    ProxyClient *partner;
    ClientStream stream;
    std::shared_ptr<ILogger> logger;
};


ProxyClient::ProxyClient(): _impl(std::make_unique<Impl>()) { }

ProxyClient::ProxyClient(IClient::io_service_type sd
        , ProxyClient::iengine_type *engine
        , const std::shared_ptr<ILogger>& logger): _impl(std::make_unique<Impl>(sd, engine, logger)) { }

void ProxyClient::set_partner(ProxyClient *proxy_client) noexcept {
    _impl->partner = proxy_client;
}

const int &ProxyClient::get_io_service() const {
    return _impl->sd;
}

void ProxyClient::set_io_service(const int &io_desc) {
    _impl->sd = io_desc;
}

proxy_client::state ProxyClient::get_state() const {
    return _impl->state;
}

void ProxyClient::set_state(const proxy_client::state &state) {
    _impl->state = state;
}

void ProxyClient::on_read(std::string &data) {
    _impl->logger->debug(utils::to_string( __func__, " called, client_sd=", _impl->sd));
    _impl->stream.append(data);
    // TODO(nickeskov): Disable logging in release mode
}

void ProxyClient::on_write() {
    _impl->logger->debug(utils::to_string( __func__, " called, client_sd=", _impl->sd));
    if (_impl->partner == nullptr) {
        on_dead();
        return;
    }

    // TODO(nickeskov): Disable logging in release mode
    while (!_impl->stream.empty()) {
        std::string data_pack = _impl->stream.get_pack();
        if (utils::send(_impl->partner->_impl->sd, data_pack) == -1) {
            _impl->logger->debug(utils::to_string("Error while sending data to partner: client_sd="
                    , _impl->sd, ", partner_sd=", _impl->partner->_impl->sd));
            on_dead();
            _impl->stream.clear();
        }
    }
}

void ProxyClient::on_dead() {
    _impl->logger->debug(utils::to_string( __func__, " called, client_sd=", _impl->sd));
    if (_impl->sd != -1) {
        shutdown(_impl->sd, SHUT_RDWR);
    }

    if (_impl->partner == nullptr) {
        return;
    }

    // TODO(nickeskov): Disable logging in release mode
    _impl->logger->debug(utils::to_string("Shutdown partner: client_sd=", _impl->sd
            , ", partner_sd=", _impl->partner->_impl->sd));
    if (_impl->partner->_impl->sd != -1) {
        shutdown(_impl->partner->_impl->sd, SHUT_RDWR);
    }
    _impl->partner->_impl->partner = nullptr;
    _impl->partner = nullptr;
}

ProxyClient::~ProxyClient() {
    ProxyClient::on_dead();
    if (_impl->sd != -1) {
        close(_impl->sd);
    }
    _impl->logger->debug(utils::to_string("Deleting client: client_sd=", _impl->sd));
}
