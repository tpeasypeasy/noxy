//
// Created by nickeskov on 07.12.2019.
//

#include <sys/epoll.h>
#include <sys/socket.h>     // socket()
#include <netinet/in.h>     // htons(), INADDR_ANY
#include <netinet/tcp.h>    // SOL_TCP

#include <cerrno>
#include <cassert>

#include <unistd.h>     // close()

#include <cstdint>     // uint32_t
#include <cstring>

#include <iostream>
#include <set>
#include <exception>
#include <stdexcept>
#include <thread>
#include <algorithm>
#include <string>
#include <vector>
#include <arpa/inet.h>

#include "noxy_core_utils.h"
#include "epoll_engine.h"
#include "proxy_client.h"


constexpr std::size_t max_read_buff_size = 65536 * 2; // 128 Kb

//template <typename io_service_type, typename client_type>
//struct EpollEngine::Impl {
//  public:
//    Impl() {
//        static_assert(std::is_base_of<IEngine::tclient_type, client_type>::value,
//                        "Your client_type not derived of client interface for current engine");
//    }
//
//    int epoll_fd = -1;
//
//    std::unordered_map<io_service_type, client_type *> ios_cli_map;
//};

void EpollEngine::stop() {
    TEngine::stop();
}

void EpollEngine::run() {
    reset_stop();
    event_loop();
}

void EpollEngine::event_loop() {

    _logger->info("Started epoll event loop");

    _epoll_fd = epoll_create1(0);
    if (_epoll_fd == -1) {
        _logger->error(utils::to_string("Cannot create epoll: ", strerror(errno)));
        return;
    }
    {
        struct epoll_event ev{};
        ev.data.fd = get_listener();
        ev.events = EPOLLIN;
        if (epoll_ctl(_epoll_fd, EPOLL_CTL_ADD, get_listener(), &ev) != 0) {
            _logger->error(utils::to_string("Cannot add listener to epoll: ", strerror(errno)));
            close(get_listener());
            return;
        }
    }

    std::vector<struct epoll_event> events(_max_clients_per_cycle);
    events.resize(_max_clients_per_cycle);

    std::vector<IEngine::iclient_type *> disconnected_clients;
    disconnected_clients.reserve(_max_clients_per_cycle);
    while (!is_stopped()) { // Enter in loop

        int epoll_ret = epoll_wait(_epoll_fd, events.data(), _max_clients_per_cycle, 200); // Infinite timeout

        if (epoll_ret == 0) {
            // TODO(nickeskov): change later epoll_wait to epoll_pwait
//            _logger->warning("epoll_wait return zero events");
            continue;
        }

        if (epoll_ret == -1) {
            if (is_stopped()) {
                break;
            }

            _logger->error(utils::to_string("epoll_wait: ", strerror(errno)));

            for (const auto &[io_service, client_ptr] : _ios_cli_map) {
                _logger->info(utils::to_string("Close: io_service=", io_service
                        , ", client_ptr=", client_ptr));
                delete client_ptr;
            }
            _ios_cli_map.clear();
            close(_epoll_fd);

            return;
        }

        for (int i = 0; i < epoll_ret; ++i) {
            if (events[i].data.fd == get_listener()) {

                if (!accept_connection()) {
                    _logger->warning("TCP tunnel creation fails");
                }

            } else if (events[i].events & EPOLLHUP) {

                auto *client = static_cast<IEngine::iclient_type *>(events[i].data.ptr);
                if (client == nullptr) {
                    _logger->fatal("Connection hup on sd, but client ptr equals nullptr");
                    // TODO(nickeskov): remove after testing
                    throw std::runtime_error("client ptr must not be nullptr");
                }
                _logger->info(utils::to_string("Connection hup on sd=", client->get_io_service()));
                disconnected_clients.push_back(client);

            } else if (events[i].events & EPOLLIN) {

                auto *client = static_cast<IEngine::iclient_type *>(events[i].data.ptr);
                if (client == nullptr) {
                    _logger->fatal("EPOLLIN on sd, but client ptr equals nullptr");
                    // TODO(nickeskov): remove after testing
                    throw std::runtime_error("client ptr must not be nullptr");
                }

                std::string buff;
                int r = utils::recv(client->get_io_service(), buff, max_read_buff_size);
                if (r == -1) {
                    if (errno != EAGAIN) {
                        _logger->info(utils::to_string("Read error on io_service="
                                , client->get_io_service(), ": ", strerror(errno)));
                        disconnected_clients.push_back(client);
                    } else {
                        _logger->warning(utils::to_string("Strange case on read with io_service="
                                , client->get_io_service(), ": ", strerror(errno)));
                    }

                } else if (r == 0) {
                    _logger->info(utils::to_string("Read EOF, disconnected, io_service="
                            , client->get_io_service(), ": ", strerror(errno)));
                    client->on_dead();

                } else if (r > 0) {
                    client->on_read(buff);
                }

                client->on_write();

            } else {
                auto *client = static_cast<IEngine::iclient_type *>(events[i].data.ptr);
                if (client == nullptr) {
                    _logger->fatal("EPOLLIN on sd, but client ptr equals nullptr");
                    // TODO(nickeskov): remove after testing
                    throw std::runtime_error("client ptr must not be nullptr");
                }

                if (events[i].events & EPOLLERR) {
                    _logger->error(utils::to_string("EPOLLER on sd="
                            , client->get_io_service(), ": ", strerror(errno)));
                } else {
                    _logger->warning(utils::to_string("Unknown event in epoll on sd="
                            , client->get_io_service()));
                }
            }
        }


        if (!disconnected_clients.empty()) {
            _logger->info("Deleting disconnected clients");
            for (auto client : disconnected_clients) {
                auto io_service = client->get_io_service();
                _logger->info(utils::to_string("Close: io_service=", io_service
                        , ", client_ptr=", client));
//                if (epoll_ctl(_epoll_fd, EPOLL_CTL_DEL, io_service, nullptr) != 0) {
//                    _logger->fatal(utils::to_string("Cannot remove client from epoll, io_service="
//                            , io_service, ", client_ptr=", client, ", err_msg: ", strerror(errno)));
//                }
                _ios_cli_map.erase(io_service);
                delete client;
            }
            disconnected_clients.clear();
        }
    }

    _logger->info("Engine stopped");
    close(_epoll_fd);
    _epoll_fd = -1;

    if (!disconnected_clients.empty()) {
        _logger->info("Deleting disconnected clients ");
        for (auto client_ptr : disconnected_clients) {
            _logger->info(utils::to_string("Close: io_service=", client_ptr->get_io_service(),
                    ", client_ptr=", client_ptr));
            _ios_cli_map.erase(client_ptr->get_io_service());
            delete client_ptr;
        }
    }
    for (const auto &[io_service, client_ptr] : _ios_cli_map) {
        _logger->info(utils::to_string("Close: io_service=", io_service
                , ", client_ptr=", client_ptr));
        delete client_ptr;
    }
    _ios_cli_map.clear();
}

bool EpollEngine::add_to_event_loop(IEngine::iclient_type *client, iengine::event_t event) {
    // TODO(nickeskov): Disable logging in release mode?
    if (client == nullptr) {
        _logger->error(utils::to_string("Client pointer is nullptr!: ", __func__));
        return false;
    }

    struct epoll_event cli_ev{};

    cli_ev.data.ptr = client;

    if (event == iengine::event_t::EV_READ) {
        cli_ev.events = EPOLLIN;
        client->set_state(iclient_type::event_type::WANT_READ);
    }
    else if (event == iengine::event_t::EV_WRITE) {
        cli_ev.events = EPOLLOUT;
        client->set_state(iclient_type::event_type::WANT_WRITE);
    }

    if (epoll_ctl(_epoll_fd, EPOLL_CTL_ADD, client->get_io_service(), &cli_ev) != 0) {
        _logger->error(utils::to_string("Cannot add client to event_loop: io_service="
                , client->get_io_service(), ", ptr=", client, ", err_msg=", strerror(errno)));
        return false;
    }

    _logger->info(utils::to_string("Added client to event_loop, client_io_service="
            , client->get_io_service()));

    return true;
}

bool EpollEngine::change_event(IEngine::iclient_type *client, iengine::event_t event) {
    if (client == nullptr) {
        _logger->error(utils::to_string("Client pointer is nullptr!: ", __func__));
        return false;
    }
    struct epoll_event ev{};

    ev.data.ptr = client;

    if (event == iengine::event_t::EV_READ) {
        ev.events = EPOLLIN;
        client->set_state(iclient_type::event_type::WANT_READ);
    }
    else if (event == iengine::event_t::EV_WRITE) {
        ev.events = EPOLLOUT;
        client->set_state(iclient_type::event_type::WANT_WRITE);
    }
    else if (event == iengine::event_t::EV_NONE) {
        _logger->info(utils::to_string("No waiting for events: io_service="
                , client->get_io_service(), ", ptr=", client));
        ev.events = 0;
    }

    if (epoll_ctl(_epoll_fd, EPOLL_CTL_MOD, client->get_io_service(), &ev) != 0) {
        _logger->error(utils::to_string("Cannot change event for: io_service="
                , client->get_io_service(), ", ptr=", client, ", err_msg=", strerror(errno)));
    }

    _logger->info(utils::to_string("Changed event for client: io_service="
            , client->get_io_service()));

    return true;
}

EpollEngine::~EpollEngine() {
    _logger->info("Deleting EpollEngine");
    for (const auto &[io_service, client_ptr] : _ios_cli_map) {
        _logger->info(utils::to_string("Close: io_service=", io_service
                , ", client_ptr=", client_ptr));
        delete client_ptr;
    }
    if (_epoll_fd != -1) {
        close(_epoll_fd);
        _epoll_fd = -1;
    }
    _logger->info("Deleted EpollEngine");
}

bool EpollEngine::accept_connection() {
    // TODO(nickeskov): Disable logging in release mode?
    struct sockaddr_in client{};
    socklen_t cli_len = sizeof(client);

    int cli_sd = accept(get_listener(), reinterpret_cast<struct sockaddr *>(&client), &cli_len);
    if (cli_sd == -1) {
        _logger->error(utils::to_string("Cannot accept connection: ", strerror(errno)));
        return false;
    }

    std::unique_ptr<IHostHolder> host_holder = get_host();
    int partner_cli_sd = create_partner_socket(*host_holder);
    if (partner_cli_sd == -1) {
        shutdown(cli_sd, SHUT_RDWR);
        close(cli_sd);
        _logger->error(utils::to_string("Cannot create partner socket: ", strerror(errno)));
        return false;
    }

    auto *proxy_client = new ProxyClient(cli_sd, this, _logger);
    auto *partner_proxy_client = new ProxyClient(partner_cli_sd, this, _logger);

    proxy_client->set_partner(partner_proxy_client);
    partner_proxy_client->set_partner(proxy_client);

    if (!add_to_event_loop(proxy_client, iengine::event_t::EV_READ)
        || !add_to_event_loop(partner_proxy_client, iengine::event_t::EV_READ)) {
        delete proxy_client;
        delete partner_proxy_client;

        _logger->error(utils::to_string("Epoll accept connection and create tcp-tunnel: "
                , strerror(errno)));
        return false;
    }

    _ios_cli_map.emplace(cli_sd, proxy_client);
    _ios_cli_map.emplace(partner_cli_sd, partner_proxy_client);

    _logger->info(utils::to_string("Epoll accept connection: cli_sd="
            , cli_sd, ", partner_sd=", partner_cli_sd));
    return true;
}

int EpollEngine::create_partner_socket(IHostHolder& host_holder) const {
    // TODO(nickeskov): Disable logging in release mode?
    int sd = socket(/*Protocol family*/PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sd == -1) {
        _logger->error(utils::to_string("Create_partner_client fails, cannot create socket: "
                , strerror(errno)));
        return -1;
    }

    const std::string ip = host_holder.get_ip();
    int port = host_holder.get_port();

    struct sockaddr_storage sa{};
    int pton_status = -1;

    if (host_holder.is_ipv6()) {
        auto *sa_ipv6 = reinterpret_cast<struct sockaddr_in6 *>(&sa);
        sa_ipv6->sin6_family = AF_INET6; // Address Family, only AF_INET6 !
        sa_ipv6->sin6_port = htons(port);
        pton_status = inet_pton(AF_INET6, ip.data(), &(sa_ipv6->sin6_addr));
    } else {
        auto *sa_ipv4 = reinterpret_cast<struct sockaddr_in *>(&sa);
        sa_ipv4->sin_family = AF_INET; // Address Family, only AF_INET !
        sa_ipv4->sin_port = htons(port);
        pton_status = inet_pton(AF_INET, ip.data(), &(sa_ipv4->sin_addr));
    }

    if (pton_status != 1) {
        if (pton_status == -1) {
            _logger->error(utils::to_string("Create_partner_client fails, ip address not valid: "
                    , strerror(errno)));
        } else if (pton_status == 0) {
            _logger->error("Create_partner_client fails, input string does not contain a character"
                           "string representing a valid network address in the specified address family.");
        }
        close(sd);
        return -1;
    }

    if (utils::set_nonblock(sd) == -1) {
        _logger->error(utils::to_string("Create_partner_client fails, "
                  "cannot change partner socket mode to nonblock mode: "
                , strerror(errno)));
        close(sd);
        return -1;
    }

    int connect_status = connect(sd, reinterpret_cast<struct sockaddr*>(&sa), sizeof(sa));
    if (connect_status == -1 && errno != EINPROGRESS) {
        _logger->error(utils::to_string("Create_partner_client fails, cannot connect to backend,  ip="
                , ip, ", port=", port, ", err_msg=", strerror(errno)));
        close(sd);
        return -1;
    }

    return sd;
}
