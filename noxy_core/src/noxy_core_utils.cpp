//
// Created by nickeskov on 07.12.2019.
//

#include <fcntl.h>
#include <string>
#include <sys/socket.h>

#include "noxy_core_utils.h"

namespace utils {
    int set_nonblock(int fd) {
         int flags;
    #ifdef O_NONBLOCK
         if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
             flags = 0;
         return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    #else
         flags = 1;
         return ioctl(fd, FIOBIO, &flags);
    #endif
    }

    int set_nonblocked(int sd, bool opt)
    {
        int flags = fcntl(sd, F_GETFL, 0);
        int new_flags = (opt) ? (flags | O_NONBLOCK) : (flags & ~O_NONBLOCK);
        return fcntl(sd, F_SETFL, new_flags);
    }

    int send(int sd, const std::string &str) noexcept
    {
        size_t left = str.size();
        ssize_t sent = 0;
        //int flags = MSG_DONTWAIT | MSG_NOSIGNAL;
        int flags = MSG_NOSIGNAL;

        while (left > 0) {
            sent = ::send(sd, str.data() + sent, str.size() - sent, flags);
            if (-1 == sent) {
                return -1;
            }
            left -= sent;
        }

        return str.size();
    }

    int recv(int sd, std::string &str, std::size_t max_buff_size) {
        str.resize(max_buff_size);
        int flags = MSG_NOSIGNAL;
        int recieved = ::recv(sd, str.data(), str.length(), flags);
        if (recieved > 0) {
            str = str.substr(0, recieved);
        } else {
            str.clear();
            str.shrink_to_fit();
        }
        return recieved;
    }
}
