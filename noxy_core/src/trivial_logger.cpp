//
// Created by nickeskov on 24.12.2019.
//

#include "trivial_logger.h"

#include <iostream>
#include <iomanip>
#include <chrono>

namespace {
    template <typename ...Args>
    constexpr void log(std::string_view prefix, Args&&... args) {
        std::cout << prefix;
        (std::cout << ... << args) << std::endl;
    }

    decltype(auto) now_time() {
        auto now = std::chrono::system_clock::now();
        auto time = std::chrono::system_clock::to_time_t(now);
        return std::put_time(std::localtime(&time), "%Y/%m/%d %T");
    }

    constexpr std::string_view trace_prefix = "[TRACE]";
    constexpr std::string_view debug_prefix = "[DEBUG]";
    constexpr std::string_view info_prefix = "[INFO]";
    constexpr std::string_view warning_prefix = "[WARNING]";
    constexpr std::string_view error_prefix = "[ERROR]";
    constexpr std::string_view fatal_prefix = "[FATAL]";
}

void TrivialLogger::trace(const std::string &str) {
    log(trace_prefix, '(', now_time(), ")| ", str);
}

void TrivialLogger::debug(const std::string &str) {
    log(debug_prefix, '(', now_time(), ")| ", str);
}

void TrivialLogger::info(const std::string &str) {
    log(info_prefix, '(', now_time(), ")| ", str);
}

void TrivialLogger::warning(const std::string &str) {
    log(warning_prefix, '(', now_time(), ")| ", str);
}

void TrivialLogger::error(const std::string &str) {
    log(error_prefix, '(', now_time(), ")| ", str);
}

void TrivialLogger::fatal(const std::string &str) {
    log(fatal_prefix, '(', now_time(), ")| ", str);
}
