//
// Created by nickeskov on 06.12.2019.
//

#ifndef NOXY_CORE_ICLIENT_H
#define NOXY_CORE_ICLIENT_H


template <typename data_t, typename io_service_t, typename event_t>
class IClient {
  public:
    using data_type = data_t;
    using io_service_type = io_service_t;
    using event_type = event_t;

    IClient() {
        static_assert(has_want_read<event_type>::value,
                "cli_event_t must have 'WANT_READ' definition");
        static_assert(has_want_write<event_type>::value,
                      "cli_event_t must have 'WANT_WRITE' definition");
    }

    virtual void on_read(data_type &data) = 0;
    virtual void on_write() = 0;
    virtual void on_dead() = 0;

    virtual void set_io_service(const io_service_type& io_desc) = 0;
    [[nodiscard]] virtual const io_service_type &get_io_service() const = 0;

    virtual void set_state(const event_type &state) = 0;
    [[nodiscard]] virtual event_type get_state() const = 0;

    virtual ~IClient() = default;

  private:

    // SFINAE checks for cli_event_t has WANT_READ field
    template <typename T>
    class has_want_read {
        typedef char yes_type[1];
        typedef char no_type[2];

        template <typename C> static yes_type &test(decltype(sizeof(C::WANT_READ)));
        template <typename C> static no_type &test(...);
    public:
        enum {
            value = sizeof(test<T>(0)) == sizeof(yes_type)
        };
    };

    // SFINAE checks for cli_event_t has WANT_WRITE field
    template <typename T>
    class has_want_write {
        typedef char yes_type[1];
        typedef char no_type[2];

        template <typename C> static yes_type &test(decltype(sizeof(C::WANT_WRITE)));
        template <typename C> static no_type &test(...);
    public:
        enum {
            value = sizeof(test<T>(0)) == sizeof(yes_type)
        };
    };
};

#endif //NOXY_CORE_ICLIENT_H
