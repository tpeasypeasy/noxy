//
// Created by nickeskov on 24.12.2019.
//

#ifndef NOXY_CORE_ILOGGER_H
#define NOXY_CORE_ILOGGER_H

#include <string>

class ILogger {
  public:
    virtual void trace(const std::string &str) = 0;

    virtual void debug(const std::string &str) = 0;

    virtual void info(const std::string &str) = 0;

    virtual void warning(const std::string &str) = 0;

    virtual void error(const std::string &str) = 0;

    virtual void fatal(const std::string &str) = 0;

    virtual ~ILogger() = default;
};

#endif //NOXY_CORE_ILOGGER_H
