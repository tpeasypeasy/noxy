//
// Created by nickeskov on 06.12.2019.
//

#ifndef NOXY_CORE_IENGINE_H
#define NOXY_CORE_IENGINE_H

#include <cinttypes>
#include <utility>

#include "iclient.h"


namespace iengine {
    enum class op_err_t: uint8_t {
        OP_EOF = 0,
        OP_HUP
    };

    enum class event_t: uint8_t {
        EV_NONE = 0,
        EV_READ,
        EV_WRITE
    };
}

template <typename data_t, typename io_service_t, typename cli_event_t>
class IEngine {
  public:
    using iclient_type = IClient<data_t, io_service_t, cli_event_t>;

    virtual void stop() = 0;
    [[nodiscard]] virtual bool is_stopped() const noexcept = 0;
    virtual void run() = 0;

    [[nodiscard]] virtual bool add_to_event_loop(iclient_type *client, iengine::event_t event) = 0;
    [[nodiscard]] virtual bool change_event(iclient_type *client, iengine::event_t event) = 0;

    virtual ~IEngine() = default;
};

#endif //NOXY_CORE_IENGINE_H
