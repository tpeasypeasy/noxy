//
// Created by nickeskov on 07.12.2019.
//

#ifndef NOXY_CORE_IABSTRACT_STREAM_H
#define NOXY_CORE_IABSTRACT_STREAM_H

#include <cstddef>

template <typename data_t>
class IAbstractStream {
  public:
    using data_type = data_t;
    using size_type = std::size_t;

    virtual void append(const data_type &data) = 0;
    virtual void append(data_type &&data) = 0;

    [[nodiscard]] virtual data_type get() const = 0;
    [[nodiscard]] virtual data_type get_pack() = 0;

    virtual void clear() = 0;
    [[nodiscard]] virtual size_type size() const noexcept = 0;
    [[nodiscard]] virtual bool empty() const noexcept = 0;

    virtual ~IAbstractStream() = default;
};

#endif //NOXY_CORE_IABSTRACT_STREAM_H
