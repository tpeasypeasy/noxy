//
// Created by nickeskov on 19.12.2019.
//

#ifndef NOXY_CORE_IDISPENSER_H
#define NOXY_CORE_IDISPENSER_H

template <typename ret_t>
class IDispenser {
  public:
    using return_type = ret_t;

    virtual return_type dispense() = 0;

    virtual ~IDispenser() = default;
};

#endif //NOXY_CORE_IDISPENSER_H
