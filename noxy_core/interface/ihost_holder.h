//
// Created by nickeskov on 19.12.2019.
//

#ifndef NOXY_CORE_IHOST_HOLDER_H
#define NOXY_CORE_IHOST_HOLDER_H

#include <string>

struct IHostHolder {
  public:
    [[nodiscard]] virtual std::string get_ip() const = 0;
    [[nodiscard]] virtual int get_port() const noexcept = 0;
    [[nodiscard]] virtual bool is_ipv6() const noexcept = 0;
    virtual ~IHostHolder() = default;
};

#endif //NOXY_CORE_IHOST_HOLDER_H
