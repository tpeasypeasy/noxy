//
// Created by nickeskov on 06.12.2019.
//

#ifndef NOXY_CORE_ISERVER_H
#define NOXY_CORE_ISERVER_H

class IServer {
  public:
    virtual void run() = 0;
    virtual void stop() = 0;

    virtual ~IServer() = default;
};

#endif //NOXY_CORE_ISERVER_H
