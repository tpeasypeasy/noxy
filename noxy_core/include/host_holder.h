//
// Created by nickeskov on 19.12.2019.
//

#ifndef NOXY_CORE_HOST_HOLDER_H
#define NOXY_CORE_HOST_HOLDER_H


#include <string>

#include "ihost_holder.h"

struct HostHolder : public IHostHolder {
  public:

    HostHolder(std::string ip, int port, bool is_ipv6);

    [[nodiscard]] std::string get_ip() const override;

    [[nodiscard]] int get_port() const noexcept override;

    [[nodiscard]] bool is_ipv6() const noexcept override;

  private:
    std::string _ip;
    int _port;
    bool _is_ipv6;
};

#endif //NOXY_CORE_HOST_HOLDER_H
