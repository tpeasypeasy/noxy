//
// Created by nickeskov on 07.12.2019.
//

#ifndef NOXY_CORE_NOXY_CORE_UTILS_H
#define NOXY_CORE_NOXY_CORE_UTILS_H

#include <exception>
#include <string>
#include <string_view>
#include <sstream>
#include <type_traits>

namespace utils {


    int set_nonblock(int fd);
    int set_nonblocked(int sd, bool opt);
    int send(int sd, const std::string &str) noexcept;
    int recv(int sd, std::string &str, std::size_t max_buff_size);

//    template <typename T>
//    std::string to_string(const T &arg) {
//        return std::is_arithmetic<T>::value ? std::to_string(arg) : std::string(arg);
//    }
//
//    std::string to_string(const char *str) {
//        return str;
//    }

    template <typename ...Args>
    std::string to_string(Args&&... args) {
        std::stringstream ss;
        (ss << ... << args);
        return ss.str();
    }
}

#endif //NOXY_CORE_NOXY_CORE_UTILS_H
