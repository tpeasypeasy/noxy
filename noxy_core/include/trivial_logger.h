//
// Created by nickeskov on 24.12.2019.
//

#ifndef NOXY_CORE_TRIVIAL_LOGGER_H
#define NOXY_CORE_TRIVIAL_LOGGER_H

#include "ilogger.h"

class TrivialLogger: public ILogger {
  public:
    void trace(const std::string &str) override;

    void debug(const std::string &str) override;

    void info(const std::string &str) override;

    void warning(const std::string &str) override;

    void error(const std::string &str) override;

    void fatal(const std::string &str) override;

    ~TrivialLogger() override = default;
};

#endif //NOXY_CORE_TRIVIAL_LOGGER_H
