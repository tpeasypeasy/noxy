//
// Created by nickeskov on 07.12.2019.
//

#ifndef NOXY_CORE_EPOLL_ENGINE_H
#define NOXY_CORE_EPOLL_ENGINE_H

#include <unordered_map>
#include <memory>
#include <utility>

#include "tengine.h"
#include "proxy_client.h"

class EpollEngine : public TEngine<proxy_client::state> {
  public:

    EpollEngine(const IHostHolder &this_host,
            std::shared_ptr<TEngine::host_dispenser_type> dispenser_ptr,
            std::shared_ptr<ILogger> logger,
            std::size_t max_clients = 4096)
        : TEngine(this_host, std::move(dispenser_ptr), std::move(logger))
        , _max_clients_per_cycle(max_clients) {}

    void stop() override;

    void run() override;

    [[nodiscard]] bool add_to_event_loop(IEngine::iclient_type *client, iengine::event_t event) override;

    [[nodiscard]] bool change_event(IEngine::iclient_type *client, iengine::event_t event) override;

    ~EpollEngine() override;

  private:
//    template <typename io_service_type, typename client_type>
//    struct Impl;
//
//    std::unique_ptr<Impl<IEngine::tclient_type::io_service_type, IEngine::tclient_type *>> _impl;

    const std::size_t _max_clients_per_cycle;

    int _epoll_fd = -1;

    std::unordered_map<IEngine::iclient_type::io_service_type, IEngine::iclient_type *> _ios_cli_map;

  private:

    void event_loop();

    bool accept_connection();

    int create_partner_socket(IHostHolder& host_holder) const;
};

#endif //NOXY_CORE_EPOLL_ENGINE_H
