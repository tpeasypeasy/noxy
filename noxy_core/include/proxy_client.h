//
// Created by nickeskov on 07.12.2019.
//

#ifndef NOXY_CORE_PROXY_CLIENT_H
#define NOXY_CORE_PROXY_CLIENT_H

#include <string>
#include <memory>
#include <cinttypes>

#include "iclient.h"
#include "iengine.h"
#include "client_stream.h"
#include "ilogger.h"


namespace proxy_client {
    enum class state: uint8_t {
        UNKNOWN = 0,
        INIT,
        WANT_CONNECT,
        WANT_WRITE_TO_TARGET,
        WANT_READ_FROM_TARGET,
        WANT_WRITE_TO_CLIENT,
        WANT_READ_FROM_CLIENT,
        WANT_READ,
        WANT_WRITE,
    };
}

class ProxyClient : public IClient<std::string, int, proxy_client::state> {
  public:
    using iengine_type = IEngine<IClient::data_type, IClient::io_service_type , IClient::event_type>;

    ProxyClient();

    ProxyClient(IClient::io_service_type sd, iengine_type *engine, const std::shared_ptr<ILogger>& logger);

    void set_partner(ProxyClient *proxy_client) noexcept;

    [[nodiscard]] const io_service_type &get_io_service() const override;

    void set_io_service(const io_service_type &io_desc) override;

    [[nodiscard]] event_type get_state() const override;

    void set_state(const event_type &state) override;

    void on_read(data_type &data) override;

    void on_write() override;

    void on_dead() override;

    ~ProxyClient() override;

  private:
    struct Impl;
    std::unique_ptr<Impl> _impl;
};


#endif //NOXY_CORE_PROXY_CLIENT_H
