//
// Created by nickeskov on 07.12.2019.
//

#ifndef NOXY_CORE_CLIENT_STREAM_H
#define NOXY_CORE_CLIENT_STREAM_H

#include <string>
#include <memory>

#include "iabstract_stream.h"

class ClientStream : public IAbstractStream<std::string> {
  public:

    ClientStream();

    void append(const data_type &data) override;

    void append(data_type &&data) override;

    [[nodiscard]] size_type size() const noexcept override;

    [[nodiscard]] bool empty() const noexcept override;

    void clear() override;

    [[nodiscard]] data_type get() const override;

    [[nodiscard]] data_type get_pack() override;

    ~ClientStream() override;

  private:
    struct Impl;
    std::unique_ptr<Impl> _impl;
};

#endif //NOXY_CORE_CLIENT_STREAM_H
